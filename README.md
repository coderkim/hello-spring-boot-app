# hello-spring-boot-app

#### 介绍
Spring Boot组件化开发学习

#### 软件架构
Gradle 4.10 + Spring Boot 2.2.x


#### 安装教程

1.  运行：gradlew bootRun
2.  打包：gradlew bootJar
3.  部署：上传jar包，java -jar xxx.jar

#### 使用说明

1.  登录：http://localhost:8085/test/login?ok=1
2.  访问：http://localhost:8085/test/hello?time=123

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
