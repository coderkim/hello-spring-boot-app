package com.github.hgkmail.hello;

import com.github.hgkmail.hello.dao.employees.SalaryDao;
import com.github.hgkmail.hello.dao.employees.SalaryStatsDao;
import com.github.hgkmail.hello.entity.employees.Salary;
import com.github.hgkmail.hello.entity.employees.SalaryStatsRecord;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(classes = {HelloSpringBootAppApplication.class})
public class EmployeeSalaryStatsTest {

    private static final Logger logger = LoggerFactory.getLogger(EmployeeSalaryStatsTest.class);

    @Autowired
    SalaryDao salaryDao;

    @Autowired
    SalaryStatsDao salaryStatsDao;

    @Test
    void test_getSalaryByEmpNo_ok() {
        List<Salary> salaries = salaryDao.getSalaryByEmpNo(10001);
        logger.info("salaries {}", salaries);
    }

    @Test
    void test_saveRecord_ok() {
        SalaryStatsRecord record = new SalaryStatsRecord(111);
        int affect = salaryStatsDao.saveRecord(record);

        assertEquals(1, affect);

        salaryStatsDao.deleteByEmpNo(111);
    }

    @Test
    void test_batchSaveRecord_ok() {
        SalaryStatsRecord record1 = new SalaryStatsRecord(123);
        SalaryStatsRecord record2 = new SalaryStatsRecord(456);
        int affect = salaryStatsDao.batchSaveRecord(Stream.of(record1, record2).collect(Collectors.toList()));

        assertEquals(2, affect);

        salaryStatsDao.deleteByEmpNo(123);
        salaryStatsDao.deleteByEmpNo(456);
    }

}
