package com.github.hgkmail.hello;

import com.github.hgkmail.hello.dao.employees.EmployeesDao;
import com.github.hgkmail.hello.dao.employees.SalaryDao;
import com.github.hgkmail.hello.entity.employees.Salary;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

//测试Stream
@SpringBootTest(classes = {HelloSpringBootAppApplication.class})
public class StreamTest {
    private static final Logger logger = LoggerFactory.getLogger(OptionalTest.class);

    @Autowired
    SalaryDao salaryDao;

    @Test
    void test_reduce_ok() {
        //reduce: 初始值ini, (累加器acc, 当前值cur)->acc和cur的操作
        List<Salary> salaries = salaryDao.getSalaryByEmpNo(10001);
        Integer total = salaries.stream().map(Salary::getSalary).map(Optional::ofNullable).map(opt->opt.orElse(0))
                .reduce(0, (acc, cur)->acc+cur);
        logger.info("10001 total: {}", total);
    }

    @Test
    void test_filter_ok() {
        List<Integer> integers = IntStream.range(0,11).boxed().filter(i->i%2==0).collect(Collectors.toList());
        logger.info("integers: {}", integers);
    }

    @Test
    void test_flatMap_ok() {
        //使用flatMap把多个流合并元素，得到一个流，而不是一串流。
        //flatMap默认有一个拆流的操作
        IntStream stream1 = IntStream.range(0,11);
        IntStream stream2 = IntStream.range(20,30);
        IntStream stream3 = IntStream.range(40,50);

        List<Integer> lst = Stream.of(stream1, stream2, stream3).flatMap(e->e.boxed()).collect(Collectors.toList());
        logger.info("lst {}", lst);
    }
}
