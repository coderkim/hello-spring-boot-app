package com.github.hgkmail.hello

import com.github.hgkmail.hello.property.MyConfig
import com.github.hgkmail.hello.util.VelocityUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

@SpringBootTest
class SpringBootSpec extends Specification {

    @Autowired
    MyConfig myConfig

    def "check my config"() {
        given: "测试数据"
        myConfig

        expect: "检查结果"
        with(myConfig) {
            greet == 'hi1'
            who == 'kim1'
            age == 51
        }
    }

    def "stub my config"() {
        given: "测试数据"
        myConfig = Stub(MyConfig)
        myConfig.getGreet() >> "hello"
        myConfig.getWho() >> "tom"
        myConfig.getAge() >> 18

        expect: "检查结果"
        verifyAll(myConfig) {  //with和verifyAll效果一样，都是检查对象多个属性的值
            greet == 'hello'
            who == 'tom'
            age == 18
        }
    }

    def "check velocity util"() {
        given: '准备数据'
        def util = new VelocityUtil()

        expect: "测试方法"
        util.camel(a) == b

        where: "参数化测试"
        a         | b
        "aaa_bbb" | "aaaBbb"
        "_bbb"    | "Bbb"
        "aaa_"    | "aaa_"
        "a_b_c"   | "aBC"
    }
}
