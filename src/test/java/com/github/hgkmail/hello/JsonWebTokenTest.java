package com.github.hgkmail.hello;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

//测试JsonWebToken
//参考文章：https://www.jianshu.com/p/e88d3f8151db
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {HelloSpringBootAppApplication.class})
public class JsonWebTokenTest {
    private static final Logger logger = LoggerFactory.getLogger(JsonWebTokenTest.class);

    private static final String APP_ID = "1001";
    private static final String USER_ID = "123123";
    private static final String USER_NAME = "tom";
    private static final String USER_PWD = "e10adc3949ba59abbe56e057f20f883e";  //md5("123456")

    private String genToken(String appId, String userId, String name, String pwd) {
        //Subject: 主体，面向的用户
        //Audience: 观众，听众，接收jwt的一方
        //claim: 自定义字段
        return JWT.create()
                .withSubject(appId)
                .withAudience(userId)
                .withClaim("name", name)
                .sign(Algorithm.HMAC256(pwd));
    }

    /**
     * 一般用户用username+password登录后，生成一个token，每个请求都要携带在header
     */
    @Test
    public void getToken() {
        String token = genToken(APP_ID, USER_ID, USER_NAME, USER_PWD);
        logger.info("token: {}", token);
        Assert.assertNotNull(token);
    }

    /**
     * 在全局filter里面校验token，校验成功为有登录态和用户id，校验失败为游客
     * 一般要准备2个注解：@NotLogin, @Login
     */
    @Test
    public void checkToken() {
        String token = genToken(APP_ID, USER_ID, USER_NAME, USER_PWD);

        try {
            //最关键的校验
            JWTVerifier verifier = JWT.require(Algorithm.HMAC256(USER_PWD)).build();
            DecodedJWT jwt = verifier.verify(token);
            logger.info("jwt: {}", jwt);

            //用户是否存在校验
            Assert.assertEquals(APP_ID, jwt.getSubject());
            Assert.assertEquals(USER_ID, jwt.getAudience().get(0));
            Assert.assertEquals(USER_NAME, jwt.getClaim("name").asString());
        } catch (IllegalArgumentException e) {
            logger.error("加密算法有误 {}", e);
            throw e;
        } catch (JWTVerificationException e) {
            logger.error("校验失败 {}", e);
            throw e;
        }
    }
}
