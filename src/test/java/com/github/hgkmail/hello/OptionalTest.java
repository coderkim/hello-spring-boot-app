package com.github.hgkmail.hello;

import com.github.hgkmail.hello.dao.employees.EmployeesDao;
import com.github.hgkmail.hello.entity.employees.Employee;
import com.github.hgkmail.hello.entity.employees.Salary;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.NoSuchElementException;
import java.util.Optional;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

//测试Optional
@SpringBootTest(classes = {HelloSpringBootAppApplication.class})
public class OptionalTest {

    private static final Logger logger = LoggerFactory.getLogger(OptionalTest.class);

    @Autowired
    EmployeesDao employeesDao;

    @Test
    void test_emptyGet_throw() {
        Optional opt = Optional.empty();
        assertThrows(NoSuchElementException.class, () -> opt.get());
    }

    @Test
    void test_default_ok() {
        //这种写法真爽，比如三元操作符和if null好多了
        //flatMap有一个get的操作，要小心
        String name1 = Optional.ofNullable(employeesDao.getByEmpNo(321))
                .map(e->e.getFirst_name()).orElse("NotSuchPerson");
        logger.info("name 321 {}", name1);

        String name2 = Optional.ofNullable(employeesDao.getByEmpNo(10002))
                .map(e->e.getFirst_name()).orElse("NotSuchPerson");
        logger.info("name 10002 {}", name2);
    }

    @Test
    void test_present_ok() {
        Optional.ofNullable(employeesDao.getByEmpNo(321))
                .map(e->e.getFirst_name()).ifPresent(str->logger.info("emp1 name {}", str));

        Optional.ofNullable(employeesDao.getByEmpNo(10002))
                .map(e->e.getFirst_name()).ifPresent(str->logger.info("emp2 name {}", str));
    }

}
