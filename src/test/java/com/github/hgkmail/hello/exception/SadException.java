package com.github.hgkmail.hello.exception;

/**
 * 伤心
 *
 * @author kimhuang
 * @date 2020/12/26
 */
public class SadException extends PersonException {

    public SadException(String message) {
        super(message);
    }

    public SadException(String message, Throwable cause) {
        super(message, cause);
    }
}
