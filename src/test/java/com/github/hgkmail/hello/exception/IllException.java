package com.github.hgkmail.hello.exception;

/**
 * 生病
 *
 * @author kimhuang
 * @date 2020/12/26
 */
public class IllException extends PersonException {

    public IllException(String message) {
        super(message);
    }

    public IllException(String message, Throwable cause) {
        super(message, cause);
    }

}
