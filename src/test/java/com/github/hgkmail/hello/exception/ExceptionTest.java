package com.github.hgkmail.hello.exception;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 测试异常
 *
 * @author kimhuang
 * @date 2020/12/26
 */
public class ExceptionTest {
    private static final Logger logger = LoggerFactory.getLogger(ExceptionTest.class);

    private int divide(int a, int b) {
        try {
            int res=a/b;
            return res;
        } catch (Exception e) {
            throw new IllException(String.format("参数:%d,%d 原因:%s", a, b, e.getMessage()), e);
        }
    }

    @Test
    public void testEx() {
        try {
            divide(1, 0);
        } catch (Exception e) {
            logger.warn(String.format("%s(错误码:10005)", e.getMessage()), e);
        }
    }

}
