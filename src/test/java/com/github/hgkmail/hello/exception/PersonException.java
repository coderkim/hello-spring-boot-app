package com.github.hgkmail.hello.exception;

/**
 * 个人异常
 *
 * @author kimhuang
 * @date 2020/12/26
 */
public class PersonException extends RuntimeException {

    public PersonException(String message) {
        super(message);
    }

    public PersonException(String message, Throwable cause) {
        super(message, cause);
    }

}
