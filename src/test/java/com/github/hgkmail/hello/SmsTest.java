package com.github.hgkmail.hello;

import com.tencentcloudapi.common.Credential;
import com.tencentcloudapi.common.exception.TencentCloudSDKException;
import com.tencentcloudapi.common.profile.ClientProfile;
import com.tencentcloudapi.sms.v20190711.SmsClient;
import com.tencentcloudapi.sms.v20190711.models.SendSmsRequest;
import com.tencentcloudapi.sms.v20190711.models.SendSmsResponse;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

//测试发送短信
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {HelloSpringBootAppApplication.class})
public class SmsTest {
    private static final Logger logger = LoggerFactory.getLogger(SmsTest.class);

    @Test
    public void send_sms() throws TencentCloudSDKException {
        //secretId 和 secretKey(不要上传到公开的git仓库)
        Credential cred = new Credential("AKIDdhuwkc4p9IZ8SkadV9bAVfX38CG8mVGo", "zqZ4CsthGqALSwtz0ngNprhDeponY6bl");
        ClientProfile clientProfile = new ClientProfile();
        clientProfile.setSignMethod("HmacSHA256");
        SmsClient client = new SmsClient(cred, "",clientProfile);
        SendSmsRequest req = new SendSmsRequest();
        String appid = "1400452461";
        req.setSmsSdkAppid(appid);
        //【短信签名】短信内容
        String sign = "kim的技术站短信";
        req.setSign(sign);
        String session = "hgk";
        req.setSessionContext(session);
        String templateID = "782444";
        req.setTemplateID(templateID);
        String[] phoneNumbers = {"+8616602049304"};
        req.setPhoneNumberSet(phoneNumbers);
        String[] templateParams = {"5678"};
        req.setTemplateParamSet(templateParams);
        SendSmsResponse res = client.SendSms(req);
        logger.info("发送短信结果: {}", SendSmsResponse.toJsonString(res));
    }

}
