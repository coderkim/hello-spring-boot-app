package com.github.hgkmail.hello;

import com.github.hgkmail.hello.dao.employees.EmployeesDao;
import com.github.hgkmail.hello.dao.imdb.TitleDao;
import com.github.hgkmail.hello.entity.employees.Employee;
import com.github.hgkmail.hello.entity.imdb.AkaTitle;
import com.github.hgkmail.hello.entity.imdb.BasicTitle;
import org.apache.shardingsphere.core.strategy.keygen.SnowflakeShardingKeyGenerator;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(classes = {HelloSpringBootAppApplication.class})
public class ShardingSphereTest {
    private static final Logger logger = LoggerFactory.getLogger(ShardingSphereTest.class);

    @Autowired
    EmployeesDao employeesDao;

    private final SnowflakeShardingKeyGenerator keyGenerator = new SnowflakeShardingKeyGenerator();

    @Test
    void test_getByEmpNo_ok() {
        Employee employee = employeesDao.getByEmpNo(10001);
        logger.info("employee10001 {}", employee);
        assertEquals(10001, employee.getEmp_no());

        employee = employeesDao.getByEmpNo(10002);
        logger.info("employee10002 {}", employee);
        assertEquals(10002, employee.getEmp_no());
    }

    @Test
    void test_insert_ok() {
        String key = keyGenerator.generateKey().toString();
        logger.info("key {}", key);

        int timestamp = (int)Math.floorDiv(System.currentTimeMillis(), 1000);
        logger.info("time {}", timestamp);

        Employee employee = employeesDao.getByEmpNo(10003);
        employee.setEmp_no(timestamp);
        employee.setFirst_name(employee.getFirst_name()+timestamp);
        int res = employeesDao.save(employee);
        assertEquals(1, res);
    }


}
