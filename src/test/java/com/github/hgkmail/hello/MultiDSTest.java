package com.github.hgkmail.hello;

import com.github.hgkmail.hello.dao.employees.EmployeesDao;
import com.github.hgkmail.hello.dao.imdb.TitleDao;
import com.github.hgkmail.hello.entity.employees.Employee;
import com.github.hgkmail.hello.entity.imdb.AkaTitle;
import com.github.hgkmail.hello.entity.imdb.BasicTitle;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {HelloSpringBootAppApplication.class})
@ActiveProfiles("unittest")
@Sql({"/schema.sql", "/data.sql"})
public class MultiDSTest {
    private static final Logger logger = LoggerFactory.getLogger(MultiDSTest.class);

    @Autowired
    EmployeesDao employeesDao;

    @Autowired
    TitleDao titleDao;

    @Test
    void test_getByEmpNo_ok() {
        Employee employee = employeesDao.getByEmpNo(10001);
        logger.info("employee {}", employee);
        assertEquals(10001, employee.getEmp_no());
    }

    @Test
    void test_getBasicByPk_ok() {
        BasicTitle basicTitle = titleDao.getBasicByPk("tt0000001");
        logger.info("basicTitle {}", basicTitle);
        assertEquals("tt0000001", basicTitle.getTconst());
    }

    @Test
    void test_getAkaByTitleId_ok() {
        List<AkaTitle> akaTitles = titleDao.getAkaByTitleId("tt0000001");
        logger.info("akaTitles {}", akaTitles);
    }

}
