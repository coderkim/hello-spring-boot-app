package com.github.hgkmail.hello;

import groovy.util.logging.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.test.context.junit4.SpringRunner;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

//测试发送邮件
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {HelloSpringBootAppApplication.class})
public class MailTest {

    private static final Logger logger = LoggerFactory.getLogger(MailTest.class);

    @Autowired
    private JavaMailSender mailSender;

    private String getNowStr() {
        LocalDateTime now = LocalDateTime.now();
        DateTimeFormatter fmt = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        return fmt.format(now);
    }

    /**
     * 发送包含简单文本的邮件
     */
    @Test
    public void test_send_simple_mail() {
        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
        simpleMailMessage.setTo(new String[]{"1043445634@qq.com", "hgkformap@gmail.com"});
        simpleMailMessage.setFrom("hgkmail@163.com");
        simpleMailMessage.setSubject(String.format("Spring Boot Mail 邮件测试【简单】%s", getNowStr()));
        simpleMailMessage.setText("这里是一段简单文本。");
        try {
            long time = System.currentTimeMillis();
            mailSender.send(simpleMailMessage);
            long usedTime = System.currentTimeMillis()-time;

            //500ms左右
            logger.info("发送文本邮件成功, 耗时{}毫秒: {}", usedTime, simpleMailMessage);
        } catch (MailException e) {
            logger.error("发送文本邮件失败: {}", e);
        }
    }
    /**
     * 发送包含HTML文本和附件的邮件
     */
    @Test
    public void test_send_mime_mail() throws MessagingException {
        MimeMessage mimeMessage = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);  //multipart=true
        helper.setTo(new String[]{"1043445634@qq.com", "hgkformap@gmail.com"});
        helper.setFrom("hgkmail@163.com");
        helper.setSubject(String.format("Spring Boot Mail 邮件测试【HTML】%s", getNowStr()));

        //富文本内容一般用<table>，兼容性好
        StringBuilder sb = new StringBuilder();
        sb.append("<h1>spring 邮件测试</h1><p style=\"color: green;\">hello!this is spring mail test。</p>");
        sb.append("<p><a href=\"http://www.w3school.com.cn\">W3School</a></p>");
        helper.setText(sb.toString(), true);  //html=true

        //附件越大，耗时越长
        FileSystemResource resource = new FileSystemResource(new File("/Users/hgkmail-mb/Downloads/星钻积分.xlsx"));
        helper.addAttachment("星钻积分.xlsx", resource);

        try {
            long time = System.currentTimeMillis();
            mailSender.send(mimeMessage);
            long usedTime = System.currentTimeMillis()-time;

            logger.info("发送富文本邮件成功, 耗时{}毫秒: {}", usedTime, mimeMessage);
        } catch (MailException e) {
            logger.error("发送富文本邮件失败: {}", e);
        }

    }

}
