package com.github.hgkmail.hello.fsm;

import com.github.hgkmail.hello.HelloSpringBootAppApplication;
import org.junit.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.squirrelframework.foundation.fsm.Condition;
import org.squirrelframework.foundation.fsm.StateMachineBuilder;
import org.squirrelframework.foundation.fsm.StateMachineBuilderFactory;

import java.util.Objects;

import static com.github.hgkmail.hello.fsm.ATMStateMachine.*;

@SpringBootTest(classes = {HelloSpringBootAppApplication.class})
public class ATMStateMachineTest {
    private static final Logger logger = LoggerFactory.getLogger(ATMStateMachineTest.class);

    private ATMStateMachine stateMachine;

    @Before
    public void setup() {
        StateMachineBuilder<ATMStateMachine, ATMState, String, ATMContext> builder = StateMachineBuilderFactory.create(
                ATMStateMachine.class, ATMState.class, String.class, ATMContext.class
        );

        builder.externalTransition().from(ATMState.Idle).to(ATMState.Loading).on(EV_CONNECTED).when(new Condition<ATMContext>() {
            @Override
            public boolean isSatisfied(ATMContext context) {
                return Objects.nonNull(context) && !context.isMaintain();
            }

            @Override
            public String name() {
                return "CheckMaintain";
            }
        }).callMethod("transitFromIdleToLoadingOnConnected");

        builder.externalTransition().from(ATMState.Loading).to(ATMState.OutOfService).on(EV_LOAD_FAIL)
                .whenMvel("CheckNetwork:::(context!=null && !context.isNetwork())")
                .callMethod("transitFromLoadingToOutOfServiceOnLoadFail");
        builder.externalTransition().from(ATMState.OutOfService).to(ATMState.Loading).on(EV_RESTART)
                .callMethod("transitFromOutOfServiceToLoadingOnRestart");
        stateMachine = builder.newStateMachine(ATMState.Idle);
    }

    @After
    public void tearDown() {
        stateMachine.terminate();
    }

    @Test
    public void testFlow() {
        ATMContext ctx = new ATMContext();
        ctx.setMaintain(false);
        ctx.setNetwork(false);

        stateMachine.start(ctx);
        ATMState curState = stateMachine.getCurrentState();
        logger.info("curState: {}", curState);
        Assert.assertEquals(ATMState.Idle, curState);

        stateMachine.fire(EV_CONNECTED, ctx);
        curState = stateMachine.getCurrentState();
        logger.info("curState: {}", curState);
        Assert.assertEquals(ATMState.Loading, curState);

        stateMachine.fire(EV_LOAD_FAIL, ctx);
        curState = stateMachine.getCurrentState();
        logger.info("curState: {}", curState);
        Assert.assertEquals(ATMState.OutOfService, curState);

        stateMachine.fire(EV_RESTART, ctx);
        curState = stateMachine.getCurrentState();
        logger.info("curState: {}", curState);
        Assert.assertEquals(ATMState.Loading, curState);

        stateMachine.fire(EV_LOAD_FAIL, ctx);
        curState = stateMachine.getCurrentState();
        logger.info("curState: {}", curState);
        Assert.assertEquals(ATMState.OutOfService, curState);
    }

}
