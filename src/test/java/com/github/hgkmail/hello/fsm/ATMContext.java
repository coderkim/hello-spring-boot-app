package com.github.hgkmail.hello.fsm;

public class ATMContext {

    private boolean isMaintain;
    private boolean isNetwork;

    public ATMContext() {
        this.isMaintain = false;
        this.isNetwork = false;
    }

    public boolean isMaintain() {
        return isMaintain;
    }

    public void setMaintain(boolean maintain) {
        isMaintain = maintain;
    }

    public boolean isNetwork() {
        return isNetwork;
    }

    public void setNetwork(boolean network) {
        isNetwork = network;
    }

    @Override
    public String toString() {
        return "ATMContext{" +
                "isMaintain=" + isMaintain +
                ", isNetwork=" + isNetwork +
                '}';
    }
}
