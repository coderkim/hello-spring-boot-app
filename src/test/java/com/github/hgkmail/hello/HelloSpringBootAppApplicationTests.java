package com.github.hgkmail.hello;

import com.github.hgkmail.hello.exception.CommonException;
import com.github.hgkmail.hello.property.MyConfig;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.*;

import java.time.Duration;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@SpringBootTest(classes = {HelloSpringBootAppApplication.class})  //不需要RunWith
class HelloSpringBootAppApplicationTests {

    private static final Logger logger = LoggerFactory.getLogger(HelloSpringBootAppApplicationTests.class);

    @Mock
//    @Autowired
    MyConfig myConfig;  //这里的myConfig是mockito mock的

    @BeforeEach  //@Before  每个test都会执行
    void beforeEach() {
        logger.info("beforeEach");

        when(myConfig.getGreet()).thenReturn("hello1");
        when(myConfig.getWho()).thenReturn("tom1");
        when(myConfig.getAge()).thenReturn(18);
    }

    @AfterEach  //@After
    void afterEach() {
        logger.info("afterEach");
    }

    @BeforeAll  //@BeforeClass  只执行一次
    static void beforeAll() {
        logger.info("beforeAll");
    }

    @AfterAll  //@AfterClass
    static void afterAll() {
        logger.info("afterAll");
    }

    @Test
    void testMyConfig1() {
        logger.info("myConfig {}", myConfig);
        assertEquals("hello1", myConfig.getGreet());
    }

    @Test
    void testMyConfig2() {
        assertEquals(18, myConfig.getAge());
    }

    @Test
    void testException() {  //assert异常
        assertThrows(CommonException.class, ()->{
            throw new CommonException("test");
        });
    }

    @Test
    void testTimeout() {  //assert超时  这里指最多3秒，超过了就测试不通过
        assertTimeout(Duration.ofSeconds(3), ()->{
            Thread.sleep(2000);
        });
    }

    //参数化测试  test方法带单个参数
    @ParameterizedTest
    @ValueSource(ints = {2,4,6,8,10})
    void testEven(int num) {
        assertEquals(0, num%2);
    }

    //参数化测试  多个参数
    @ParameterizedTest
    @CsvSource({"abc,3","mary,4","hello,5"})
    void testStringLen(String str, int num) {
        assertEquals(num, str.length());
    }

    //使用AssertJ测试一个对象的多个属性
    @Test
    void testProperty() {
        assertThat(myConfig)
                .extracting("greet", "who", "age")
                .containsExactly("hello1","tom1", 18);
    }

}
