package com.github.hgkmail.hello.service;

import com.github.hgkmail.hello.dao.employees.EmployeesDao;
import com.github.hgkmail.hello.dao.employees.SalaryDao;
import com.github.hgkmail.hello.dao.employees.SalaryStatsDao;
import com.github.hgkmail.hello.entity.employees.Employee;
import com.github.hgkmail.hello.entity.employees.Salary;
import com.github.hgkmail.hello.entity.employees.SalaryStatsRecord;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.sql.Date;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class SalaryStatsServiceTest {

    @Mock
    EmployeesDao employeesDao;

    @Mock
    SalaryDao salaryDao;

    @Mock
    SalaryStatsDao salaryStatsDao;

    @InjectMocks
    private SalaryStatsService salaryStatsServiceUnderTest;

    @Before
    public void setUp() {
        // 使用MockitoJUnitRunner后，这句话就不是必须的，每个Test都会自动执行一次initMocks
        //MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testSimpleStats() {
        // Setup

        // Configure EmployeesDao.getByPage(...).
        final Employee employee = new Employee();
        employee.setEmp_no(0);
        employee.setBirth_date(new Date(0L));
        employee.setFirst_name("first_name");
        employee.setLast_name("last_name");
        employee.setGender("gender");
        employee.setHire_date(new Date(0L));
        final List<Employee> employees = Arrays.asList(employee);
        when(salaryStatsServiceUnderTest.employeesDao.getByPage(0, 0)).thenReturn(employees);

        // Configure SalaryDao.getSalaryByEmpNo(...).
        final Salary salary = new Salary();
        salary.setEmp_no(0);
        salary.setSalary(0);
        salary.setFrom_date(new Date(0L));
        salary.setTo_date(new Date(0L));
        final List<Salary> salaries = Arrays.asList(salary);
        when(salaryStatsServiceUnderTest.salaryDao.getSalaryByEmpNo(0)).thenReturn(salaries);

        when(salaryStatsServiceUnderTest.salaryStatsDao.saveRecord(any(SalaryStatsRecord.class))).thenReturn(0);

        // Run the test
        final int result = salaryStatsServiceUnderTest.simpleStats();

        // Verify the results
        assertEquals(0, result);
    }

    @Test
    public void testMultiThreadStats() {
        // Setup
        when(salaryStatsServiceUnderTest.employeesDao.countAll()).thenReturn(0);

        // Configure EmployeesDao.getByPage(...).
        final Employee employee = new Employee();
        employee.setEmp_no(0);
        employee.setBirth_date(new Date(0L));
        employee.setFirst_name("first_name");
        employee.setLast_name("last_name");
        employee.setGender("gender");
        employee.setHire_date(new Date(0L));
        final List<Employee> employees = Arrays.asList(employee);
        when(salaryStatsServiceUnderTest.employeesDao.getByPage(0, 0)).thenReturn(employees);

        when(salaryStatsServiceUnderTest.salaryStatsDao.deleteBefore(0)).thenReturn(0);

        // Run the test
        final int result = salaryStatsServiceUnderTest.multiThreadStats();

        // Verify the results
        assertEquals(0, result);
    }

    @Test
    public void testProducerConsumerStats() {
        // Setup
        when(salaryStatsServiceUnderTest.employeesDao.countAll()).thenReturn(1);

        // Configure EmployeesDao.getByPage(...).
        final Employee employee = new Employee();
        employee.setEmp_no(0);
        employee.setBirth_date(new Date(0L));
        employee.setFirst_name("first_name");
        employee.setLast_name("last_name");
        employee.setGender("gender");
        employee.setHire_date(new Date(0L));
        final List<Employee> employees = Arrays.asList(employee);
        when(salaryStatsServiceUnderTest.employeesDao.getByPage(0, 0)).thenReturn(employees);

        when(salaryStatsServiceUnderTest.salaryStatsDao.deleteBefore(0)).thenReturn(0);

        // Run the test
        final int result = salaryStatsServiceUnderTest.producerConsumerStats();

        // Verify the results
        assertEquals(0, result);
    }

    @Test
    public void testDeleteOldRecords() {
        // Setup

        // Configure EmployeesDao.getByPage(...).
        final Employee employee = new Employee();
        employee.setEmp_no(0);
        employee.setBirth_date(new Date(0L));
        employee.setFirst_name("first_name");
        employee.setLast_name("last_name");
        employee.setGender("gender");
        employee.setHire_date(new Date(0L));
        final List<Employee> employees = Arrays.asList(employee);
        when(salaryStatsServiceUnderTest.employeesDao.getByPage(0, 0)).thenReturn(employees);

        when(salaryStatsServiceUnderTest.salaryStatsDao.deleteBefore(0)).thenReturn(0);

        // Run the test
        salaryStatsServiceUnderTest.deleteOldRecords();

        // Verify the results
    }
}
