package com.github.hgkmail.hello;

import com.github.hgkmail.hello.service.SalaryStatsService;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = {HelloSpringBootAppApplication.class})
public class EmployeeBusinessTest {
    private static final Logger logger = LoggerFactory.getLogger(EmployeeBusinessTest.class);

    @Autowired
    SalaryStatsService salaryStatsService;

    @Test
    void test_simpleStats_ok() {
        int recordCount = salaryStatsService.simpleStats();
        logger.info("recordCount {}", recordCount);
    }

    @Test
    void test_producerConsumerStats_ok() {
        int recordCount = salaryStatsService.producerConsumerStats();
        logger.info("recordCount {}", recordCount);
    }

    @Test
    void test_multiThreadStats_ok() {
        int recordCount = salaryStatsService.multiThreadStats();
        logger.info("recordCount {}", recordCount);
    }

}
