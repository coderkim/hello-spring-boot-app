package com.github.hgkmail.hello;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

//GuavaCache和ConcurrentMap
//put get invalidate
public class CacheTest {
    private static final Logger logger = LoggerFactory.getLogger(CacheTest.class);

    private static LoadingCache<String, Object> cache;
    private static ConcurrentMap<String, Object> map;

    @BeforeAll  //@BeforeClass  只执行一次
    static void beforeAll() {
        cache = CacheBuilder.newBuilder()
                .initialCapacity(100)
                .concurrencyLevel(5)
                .expireAfterWrite(10, TimeUnit.SECONDS)
                .build(new CacheLoader<String, Object>() {
                    @Override
                    public Object load(String key) throws Exception {
                        return "default";
                    }
                });

        //0.75是一个官方建议值
        map = new ConcurrentHashMap<>(100, 0.75f, 5);
    }

    @Test
    void test_map_ok() {
        map.put("hello", "world");
        Object val = map.getOrDefault("hello", "default");
        logger.info("val {}", val);
        //size是map的真实大小
        logger.info("size {}", map.size());
        //get或remove不存在的key并不会抛错，而是返回null
        Object hi = map.get("hi");
        logger.info("hi {}", hi);
    }

    @Test
    void test_cache_ok() {
        cache.put("hello1", "world1");
        logger.info("hello1 {}", cache.getIfPresent("hello1"));
        cache.invalidate("hello1");
        logger.info("hello1 after invalidate {}", cache.getIfPresent("hello1"));
        try {
            //loading cache pattern: if cached, return; otherwise create, cache and return.
            logger.info("hello2 {}", cache.get("hello2"));
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        logger.info("size {}", cache.size());
    }

}
