package com.github.hgkmail.hello;

import com.github.hgkmail.hello.dao.employees.SalaryDao;
import com.github.hgkmail.hello.entity.employees.Salary;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import org.apache.commons.collections.CollectionUtils;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.*;
import java.util.concurrent.*;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

//普通ThreadPool（Executor框架）、Future(异步模式)、ForkJoinPool（Fork/Join框架）、ThreadLocal
@SpringBootTest(classes = {HelloSpringBootAppApplication.class})
public class ThreadTest {
    private static final Logger logger = LoggerFactory.getLogger(ThreadTest.class);

    //ThreadLocal一般要定义为static（阿里巴巴java手册），最好再加上private和final
    //ThreadLocal看起来只有一份，其实是每个线程都有一份，是线程全局变量
    private static final ThreadLocal<Integer> sumThreadLocal = ThreadLocal.withInitial(()-> 0);

    @Autowired
    SalaryDao salaryDao;

    private class SumSalaryTask1 implements Callable<Integer> {

        int startEmpNo;
        int endEmpNo;
        CountDownLatch latch;

        SumSalaryTask1(CountDownLatch latch, int startEmpNo, int endEmpNo) {
            this.startEmpNo = startEmpNo;
            this.endEmpNo = endEmpNo;
            this.latch = latch;
        }

        @Override
        public Integer call() throws Exception {
            for (int i = startEmpNo; i < endEmpNo; i++) {
                List<Salary> salaries = salaryDao.getSalaryByEmpNo(i);
                if (CollectionUtils.isNotEmpty(salaries)) {
                    Integer sum = sumThreadLocal.get();
                    sum += salaries.stream().map(Salary::getSalary).reduce(0,(acc,cur)->acc+cur);
                    sumThreadLocal.set(sum);
                }
            }
            latch.countDown(); //-1

            Integer sum = sumThreadLocal.get();
            sumThreadLocal.set(0);
            return sum;
        }
    }

    private class SumSalaryTask2 extends RecursiveAction {

        List<Integer> startEmpNoList;
        Map<Integer, Integer> results;
        int start; //inclusive
        int end; //exclusive

        SumSalaryTask2(List<Integer> startEmpNoList, int start, int end, Map<Integer, Integer> results) {
            this.startEmpNoList = startEmpNoList;
            this.start = start;
            this.end = end;
            this.results = results;
        }

        @Override
        protected void compute() {
            if (end-start<=0) {
                return;
            } else if (end-start==1) {
                int sum=0;
                Integer startEmpNo = startEmpNoList.get(start);
                Integer endEmpNo = startEmpNo+100;
                for (int i = startEmpNo; i < endEmpNo; i++) {
                    List<Salary> salaries = salaryDao.getSalaryByEmpNo(i);
                    if (CollectionUtils.isNotEmpty(salaries)) {
                        sum += salaries.stream().map(Salary::getSalary).reduce(0,(acc,cur)->acc+cur);
                    }
                }
                results.put(startEmpNo, sum);
            } else {
                // 任务分解
                // 将大任务分解成两个小任务
                int middle = (start + end) / 2;
                SumSalaryTask2 left = new SumSalaryTask2(startEmpNoList, start, middle, results);
                SumSalaryTask2 right = new SumSalaryTask2(startEmpNoList, middle, end, results);
                // 并行执行两个小任务
                left.fork();
                right.fork();
            }
        }
    }

    @Test
    void test_ThreadPool_ok() {
        ExecutorService pool = new ThreadPoolExecutor(11, 11, 5, TimeUnit.SECONDS,
                new ArrayBlockingQueue<>(10000),
                new ThreadFactoryBuilder().setNameFormat("test_ThreadPool_ok-pool-%d").build());

        List<Integer> startEmpNoArray = IntStream.rangeClosed(1, 1000).boxed().map(i->i*100).map(i->i+10000)
                .collect(Collectors.toList());
        CountDownLatch latch = new CountDownLatch(startEmpNoArray.size());
        List<Future<Integer>> futureList = new ArrayList<>();
        long startTime = System.currentTimeMillis();
        for (int startEmpNo:startEmpNoArray) {
            SumSalaryTask1 task = new SumSalaryTask1(latch, startEmpNo, startEmpNo+100);
            Future<Integer> future = pool.submit(task);
            futureList.add(future);
        }
        try {
            latch.await();

            long endTime = System.currentTimeMillis();
            logger.info("time {}ms", endTime-startTime);

            for (Future<Integer> f:futureList) {
                logger.info("{}", f.get());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void test_ForkJoinPool_ok() {
        List<Integer> startEmpNoArray = IntStream.rangeClosed(1, 1000).boxed().map(i->i*100).map(i->i+10000)
                .collect(Collectors.toList());

        ForkJoinPool pool = ForkJoinPool.commonPool();
        long startTime = System.currentTimeMillis();
        ConcurrentHashMap<Integer, Integer> map = new ConcurrentHashMap<Integer, Integer>(1000, 0.75f, Runtime.getRuntime().availableProcessors());
        ForkJoinTask task = pool.submit(new SumSalaryTask2(startEmpNoArray, 0, startEmpNoArray.size(), map));

        try {
            pool.awaitTermination(1, TimeUnit.MINUTES);
            pool.shutdown();
            logger.info("res {}", task.get());

            long endTime = System.currentTimeMillis();
            logger.info("time {}ms", endTime-startTime);

            Set keySet = map.keySet();
            Object[] keyArr = keySet.toArray();
            Arrays.sort(keyArr);
            for (Object key:keyArr) {
                logger.info("key {}, value {}", key, map.get(key));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
