package com.github.hgkmail.hello;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mindrot.jbcrypt.BCrypt;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

//JBCrypt：方便的密码管理工具类
//参考文章：https://segmentfault.com/a/1190000016871371
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {HelloSpringBootAppApplication.class})
public class BCryptTest {
    private static final Logger logger = LoggerFactory.getLogger(BCryptTest.class);

    //数据库不能保存明文密码，只能保存密码的hash和salt
    @Test
    public void getAndCheckPwd() {
        String salt = BCrypt.gensalt(5);
        logger.info("salt {}", salt);
        //hash=$盐版本 + $盐次数 + $真正的盐 + 真正的hash
        //数据库就不用分开存hash和salt
        String hash = BCrypt.hashpw("123456", salt);
        logger.info("hash {}", hash);
        Boolean res = BCrypt.checkpw("123456", hash);
        logger.info("check {}", res);
        Assert.assertTrue(res);
    }
}
