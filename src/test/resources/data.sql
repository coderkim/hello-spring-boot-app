INSERT INTO `employees`(`emp_no`, `birth_date`, `first_name`, `last_name`, `gender`, `hire_date`)
VALUES (10001, '1953-09-02', 'Georgi1', 'Facello', 'M', '1986-06-26');
INSERT INTO `employees`(`emp_no`, `birth_date`, `first_name`, `last_name`, `gender`, `hire_date`)
VALUES (10002, '1964-06-02', 'Bezalel2', 'Simmel', 'F', '1985-11-21');
INSERT INTO `employees`(`emp_no`, `birth_date`, `first_name`, `last_name`, `gender`, `hire_date`)
VALUES (10003, '1959-12-03', 'Parto3', 'Bamford', 'M', '1986-08-28');

INSERT INTO `t_title_basics`(`tconst`, `titleType`, `primaryTitle`, `originalTitle`, `isAdult`, `startYear`, `endYear`, `runtimeMinutes`, `genres`)
VALUES ('tt0000001', 'short', 'Carmencita1', 'Carmencita2', 0, 1894, NULL, 1, 'Documentary,Short');
