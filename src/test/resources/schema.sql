-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
-- DROP TABLE IF EXISTS `sys_menu`;
-- CREATE TABLE `sys_menu` (
--   `menu_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '菜单ID',
--   `menu_name` varchar(50) NOT NULL COMMENT '菜单名称',
--   `parent_id` bigint(20) DEFAULT '0' COMMENT '父菜单ID',
--   `order_num` int(4) DEFAULT '0' COMMENT '显示顺序',
--   `url` varchar(200) DEFAULT '#' COMMENT '请求地址',
--   `target` varchar(20) DEFAULT '' COMMENT '打开方式（menuItem页签 menuBlank新窗口）',
--   `menu_type` char(1) DEFAULT '' COMMENT '菜单类型（M目录 C菜单 F按钮）',
--   `visible` char(1) DEFAULT '0' COMMENT '菜单状态（0显示 1隐藏）',
--   `perms` varchar(100) DEFAULT NULL COMMENT '权限标识',
--   `icon` varchar(100) DEFAULT '#' COMMENT '菜单图标',
--   `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
--   `create_time` datetime DEFAULT NULL COMMENT '创建时间',
--   `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
--   `update_time` datetime DEFAULT NULL COMMENT '更新时间',
--   `remark` varchar(500) DEFAULT '' COMMENT '备注',
--   PRIMARY KEY (`menu_id`)
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='菜单权限表';

-- ----------------------------
-- Table structure for t_title_akas
-- ----------------------------
DROP TABLE IF EXISTS `t_title_akas`;
CREATE TABLE `t_title_akas` (
  `titleId` varchar(50) NOT NULL,
  `ordering` int(11) NOT NULL,
  `title` text,
  `region` varchar(20) DEFAULT NULL,
  `language` varchar(20) DEFAULT NULL,
  `types` varchar(50) DEFAULT NULL,
  `attributes` varchar(255) DEFAULT NULL,
  `isOriginalTitle` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`titleId`,`ordering`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for t_title_basics
-- ----------------------------
DROP TABLE IF EXISTS `t_title_basics`;
CREATE TABLE `t_title_basics` (
  `tconst` varchar(20) NOT NULL,
  `titleType` varchar(20) DEFAULT NULL,
  `primaryTitle` text,
  `originalTitle` text,
  `isAdult` tinyint(4) DEFAULT NULL,
  `startYear` int(11) DEFAULT NULL,
  `endYear` int(11) DEFAULT NULL,
  `runtimeMinutes` int(11) DEFAULT NULL,
  `genres` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`tconst`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for employees
-- ----------------------------
DROP TABLE IF EXISTS `employees`;
CREATE TABLE `employees` (
  `emp_no` int(11) NOT NULL,
  `birth_date` date NOT NULL,
  `first_name` varchar(14) NOT NULL,
  `last_name` varchar(16) NOT NULL,
  `gender` enum('M','F') NOT NULL,
  `hire_date` date NOT NULL,
  PRIMARY KEY (`emp_no`),
  KEY `idx_first_name` (`first_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for salaries
-- ----------------------------
DROP TABLE IF EXISTS `salaries`;
CREATE TABLE `salaries` (
  `emp_no` int(11) NOT NULL,
  `salary` int(11) NOT NULL,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL,
  PRIMARY KEY (`emp_no`,`from_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for t_salary_stats
-- ----------------------------
DROP TABLE IF EXISTS `t_salary_stats`;
CREATE TABLE `t_salary_stats` (
  `emp_no` int(11) NOT NULL COMMENT '员工号',
  `start_date` date NOT NULL DEFAULT '1970-01-01' COMMENT '入职日期',
  `end_date` date NOT NULL DEFAULT '1970-01-01' COMMENT '离职日期',
  `total_salary` decimal(19,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '总收入',
  `average_salary` decimal(19,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '年平均收入',
  `year_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '工作年数',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`emp_no`),
  KEY `idx_total_salary` (`total_salary`) USING BTREE,
  KEY `idx_year_num` (`year_num`) USING BTREE,
  KEY `idx_average_salary` (`average_salary`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='员工薪水统计';
