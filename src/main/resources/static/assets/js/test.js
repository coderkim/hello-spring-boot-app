(function($){

    var dt = new Date()
    var ts = Math.round(dt.getTime()/1000)
    $.ajax({
        url:"/hello/test/hello?time="+ts,
        dataType:"json",
        async:true,
        data:'',
        type:"GET",
        success:function(res){
            console.log(res)
            $('#p1').text(res.data)
        },
        error:function(e){
            console.log(e)
        }
    })

    $('#btn-upload').click(function () {
        var formData = new FormData()
        formData.append("file", $('#form-upload-input')[0].files[0])
        $.ajax({
            type: 'POST',
            url: '/hello/test/formUpload',
            data: formData,
            async: true,
            contentType: false,
            processData: false,
            cache: false,
            success:function(res){
                console.log(res)
            },
            error:function(e){
                console.log(e)
            }
        });
    })

    layui.use('table', function(){
        var table = layui.table;

        //第一个实例
        var tableIns = table.render({
            elem: '#demoTable'
            ,height: 300
            ,url: '/hello/test/layui/table' //数据接口
            ,toolbar: true
            ,page: true //开启分页
            ,defaultToolbar: ['filter', 'exports', 'print']
            ,cols: [[ //表头
                {field: 'id', title: 'ID', width:80, sort: true, fixed: 'left'}
                ,{field: 'name', title: '菜单名', width:200}
                ,{field: 'parentId', title: '父ID', width:80}
                ,{field: 'order', title: '顺序', width:80, sort: true}
                ,{field: 'url', title: '链接'}
                ,{fixed: 'right', title: '操作', toolbar: '#barDemo', width:150}
            ]]
        })

        //监听行工具事件
        table.on('tool(test)', function(obj){
            var data = obj.data;  //数据表格行数据
            //console.log(obj)
            if(obj.event === 'del'){
                layer.confirm('真的删除行么', function(index){
                    layer.close(index);
                });
            } else if(obj.event === 'edit'){
                var index=layer.open({
                    type: 2  //type:2  iframe
                    ,content: ["/hello/form.html?id="+data.id] //通过url传递参数
                    ,area: ['640px', '480px']
                    // ,btn: ['确定', '取消']
                    // ,yes: function () { //确定按钮事件
                    //     layer.close(index)
                    //     tableIns.reload()
                    // }
                });
            }
        })
    })

})(jQuery)

function reloadTable() {
    layui.use('table', function() {
        var table = layui.table;
        table.reload('demoTable')
    })
}