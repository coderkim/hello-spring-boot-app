package pkg1.pkg2;

//用于class loader测试
public class SimpleKey {
    private String key="123456";

    @Override
    public String toString() {
        return "SimpleKey{" +
                "key='" + key + '\'' +
                '}';
    }

}
