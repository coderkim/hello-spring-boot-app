package com.github.hgkmail.hello.shiro;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;

/**
 * 自定义域
 * 域--负责【登录】和【权限】的服务，比如kugou域就是负责kugou内网的登录和权限
 *
 * CustomRealm是CachingRealm--域信息存储在内存中
 */
public class CustomRealm extends AuthorizingRealm {

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        SimpleAuthorizationInfo authorInfo = new SimpleAuthorizationInfo();
        authorInfo.addRole("SuperAdmin");
        authorInfo.addRole("User");
        authorInfo.addStringPermission("user.login");
        authorInfo.addStringPermission("admin.edit");
        return authorInfo;
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
        //Principal--账号，Credentials--密码，realmName--域名称
        return new SimpleAuthenticationInfo(token.getPrincipal(), token.getCredentials(), getName());
    }
}
