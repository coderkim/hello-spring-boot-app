package com.github.hgkmail.hello.util;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class VelocityUtil {

    private Map<String, String> javaTypeByDbType = new HashMap<>();
    public static final String TYPE_INTEGER = "Integer";
    public static final String TYPE_LONG = "Long";
    public static final String TYPE_STRING = "String";
    public static final String TYPE_OBJECT = "Object";

    public VelocityUtil() {
        javaTypeByDbType.put("bigint", TYPE_LONG);
        javaTypeByDbType.put("int", TYPE_INTEGER);
        javaTypeByDbType.put("integer", TYPE_INTEGER);
        javaTypeByDbType.put("tinyint", TYPE_INTEGER);
        javaTypeByDbType.put("char", TYPE_STRING);
        javaTypeByDbType.put("varchar", TYPE_STRING);
        javaTypeByDbType.put("text", TYPE_STRING);
    }

    public static int plus(int a, int b) {
        return a+b;
    }

    public String getTableSchema(String schema, String table) {
        return String.format(
                "select * from information_schema.columns where table_schema ='%s' and table_name = '%s' order by ORDINAL_POSITION;",
                schema, table);
    }

    public String getJavaTypeByDbType(String dbType) {
        if (javaTypeByDbType.containsKey(dbType)) {
            return javaTypeByDbType.get(dbType);
        }
        return TYPE_OBJECT;
    }

    public String camel(String str) {
        return doCamel(new StringBuffer(str)).toString();
    }

    public String underline(String str) {
        return doUnderline(new StringBuffer(str)).toString();
    }

    /**
     * 下划线转驼峰
     * @param str
     * @return
     */
    private StringBuffer doCamel(StringBuffer str) {
        //利用正则删除下划线，把下划线后一位改成大写
        Pattern pattern = Pattern.compile("_(\\w)");
        Matcher matcher = pattern.matcher(str);
        StringBuffer sb = new StringBuffer(str);
        if(matcher.find()) {
            sb = new StringBuffer();
            //将当前匹配子串替换为指定字符串，并且将替换后的子串以及其之前到上次匹配子串之后的字符串段添加到一个StringBuffer对象里。
            //正则之前的字符和被替换的字符
            matcher.appendReplacement(sb, matcher.group(1).toUpperCase());
            //把之后的也添加到StringBuffer对象里
            matcher.appendTail(sb);
        }else {
            return sb;
        }
        return doCamel(sb);
    }

    /**
     * 驼峰转下划线
     * @param str
     * @return
     */
    private StringBuffer doUnderline(StringBuffer str) {
        Pattern pattern = Pattern.compile("[A-Z]");
        Matcher matcher = pattern.matcher(str);
        StringBuffer sb = new StringBuffer(str);
        if(matcher.find()) {
            sb = new StringBuffer();
            //将当前匹配子串替换为指定字符串，并且将替换后的子串以及其之前到上次匹配子串之后的字符串段添加到一个StringBuffer对象里。
            //正则之前的字符和被替换的字符
            matcher.appendReplacement(sb,"_"+matcher.group(0).toLowerCase());
            //把之后的也添加到StringBuffer对象里
            matcher.appendTail(sb);
        }else {
            return sb;
        }
        return doUnderline(sb);
    }

}
