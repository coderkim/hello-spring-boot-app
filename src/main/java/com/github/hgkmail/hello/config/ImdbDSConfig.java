package com.github.hgkmail.hello.config;

import com.github.hgkmail.hello.plugins.CatMybatisInterceptor;
import com.github.hgkmail.hello.plugins.CatMybatisPlugin;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import javax.sql.DataSource;

@Configuration //注册到springboot 容器中
@MapperScan(basePackages = "com.github.hgkmail.hello.dao.imdb",
        sqlSessionTemplateRef  = "imdbSqlSessionTemplate")
public class ImdbDSConfig {

    @Value("${spring.datasource.imdb.jdbc-url}")
    private String jdbcUrl;

    @Bean(name = "imdbDataSource")
    @ConfigurationProperties(prefix = "spring.datasource.imdb")
    public DataSource testDataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean(name = "imdbSqlSessionFactory")
    public SqlSessionFactory testSqlSessionFactory
            (@Qualifier("imdbDataSource") DataSource dataSource) throws Exception {
        SqlSessionFactoryBean bean = new SqlSessionFactoryBean();
        bean.setDataSource(dataSource);
        //添加CAT插件
//        CatMybatisPlugin.addCatMybatisPlugin(bean);
//        CatMybatisInterceptor.addCatMybatisInterceptor(bean, jdbcUrl);
        return bean.getObject();
    }

    @Bean(name = "imdbTransactionManager")
    public DataSourceTransactionManager testTransactionManager
            (@Qualifier("imdbDataSource") DataSource dataSource) {
        return new DataSourceTransactionManager(dataSource);
    }

    @Bean(name = "imdbSqlSessionTemplate")
    public SqlSessionTemplate testSqlSessionTemplate
            (@Qualifier("imdbSqlSessionFactory") SqlSessionFactory sqlSessionFactory) throws Exception {
        return new SqlSessionTemplate(sqlSessionFactory);
    }
}
