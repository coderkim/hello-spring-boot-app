package com.github.hgkmail.hello.config;

import com.github.hgkmail.hello.plugins.CatMybatisInterceptor;
import com.github.hgkmail.hello.plugins.CatMybatisPlugin;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.shardingsphere.api.config.sharding.ShardingRuleConfiguration;
import org.apache.shardingsphere.api.config.sharding.TableRuleConfiguration;
import org.apache.shardingsphere.api.config.sharding.strategy.InlineShardingStrategyConfiguration;
import org.apache.shardingsphere.shardingjdbc.api.ShardingDataSourceFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

@Configuration //注册到springboot 容器中
@MapperScan(basePackages = "com.github.hgkmail.hello.dao.employees",
        sqlSessionTemplateRef  = "employeesSqlSessionTemplate")
public class EmployeesDSConfig {

    @Value("${spring.datasource.employees.jdbc-url}")
    private String jdbcUrl;

    /**
     * 配置ShardingSphere数据源0，数据源的名称最好要有一定的规则，方便配置分库的计算规则
     */
    @Bean(name = "dataSourceEmployees")
    @ConfigurationProperties(prefix = "spring.shardingsphere.datasource.employees")
    public DataSource dataSourceEmployees() {
        return DataSourceBuilder.create().build();
    }

    /**
     * 关键配置，返回一个ShardingSphere数据源，通过这个DataSource实现分表
     * 和Proxy模式差不多，这里代理的是dataSource！
     */
    @Bean(name = "employeesDataSource")
    @ConfigurationProperties(prefix = "spring.datasource.employees")
    public DataSource testDataSource() throws SQLException {
        // 配置分片规则
        ShardingRuleConfiguration shardingRuleConfig = new ShardingRuleConfiguration();

        // 配置真实数据源
        Map<String, DataSource> dataSourceMap = new HashMap<>();
        dataSourceMap.put("employees", dataSourceEmployees());

        TableRuleConfiguration tableRuleConfig = new TableRuleConfiguration("employees",
                "employees.t_employees_${0..3}");
        // 配置分库 + 分表策略
        tableRuleConfig.setTableShardingStrategyConfig(new InlineShardingStrategyConfiguration("emp_no", "t_employees_${emp_no % 4}"));
        shardingRuleConfig.getTableRuleConfigs().add(tableRuleConfig);
        // 获取数据源对象
        Properties props = new Properties();
        props.setProperty("sql.show", "true");
        DataSource dataSource = ShardingDataSourceFactory.createDataSource(dataSourceMap, shardingRuleConfig, new Properties());
        return dataSource;
    }

    @Bean(name = "employeesSqlSessionFactory")
    public SqlSessionFactory testSqlSessionFactory
            (@Qualifier("employeesDataSource") DataSource dataSource) throws Exception {
        SqlSessionFactoryBean bean = new SqlSessionFactoryBean();
        bean.setDataSource(dataSource);
        //添加CAT插件
//        CatMybatisPlugin.addCatMybatisPlugin(bean);
//        CatMybatisInterceptor.addCatMybatisInterceptor(bean, jdbcUrl);
        return bean.getObject();
    }

    @Bean(name = "employeesTransactionManager")
    public DataSourceTransactionManager testTransactionManager
            (@Qualifier("employeesDataSource") DataSource dataSource) {
        return new DataSourceTransactionManager(dataSource);
    }

    @Bean(name = "employeesSqlSessionTemplate")
    public SqlSessionTemplate testSqlSessionTemplate
            (@Qualifier("employeesSqlSessionFactory") SqlSessionFactory sqlSessionFactory) throws Exception {
        return new SqlSessionTemplate(sqlSessionFactory);
    }
}
