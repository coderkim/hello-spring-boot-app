package com.github.hgkmail.hello.config;

import com.dianping.cat.servlet.CatFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by zhengwenzhu on 2017/1/17.
 */

//@Configuration
public class CatFilterConfigure {

//    @Bean
    public FilterRegistrationBean catFilter() {
        FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.addUrlPatterns("/*");
        //查看CatFilter的源码可以看到exclude可以配置排除的url，用英文分号分割
        registration.addInitParameter("exclude", "/assets/*;/test.html");
        registration.setName("cat-filter");
        registration.setOrder(1);

        CatFilter filter = new CatFilter();
        registration.setFilter(filter);
        return registration;
    }
}