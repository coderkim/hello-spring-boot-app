package com.github.hgkmail.hello.config;

import com.github.hgkmail.hello.plugins.CatMybatisInterceptor;
import com.github.hgkmail.hello.plugins.CatMybatisPlugin;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import javax.sql.DataSource;

@Configuration //注册到springboot 容器中
@MapperScan(basePackages = "com.github.hgkmail.hello.dao.edusoho",
        sqlSessionTemplateRef  = "edusohoSqlSessionTemplate")
public class EdusohoDSConfig {

    @Value("${spring.datasource.edusoho.jdbc-url}")
    private String jdbcUrl;

    @Bean(name = "edusohoDataSource")
    @ConfigurationProperties(prefix = "spring.datasource.edusoho")
    public DataSource testDataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean(name = "edusohoSqlSessionFactory")
    public SqlSessionFactory testSqlSessionFactory
            (@Qualifier("edusohoDataSource") DataSource dataSource) throws Exception {
        SqlSessionFactoryBean bean = new SqlSessionFactoryBean();
        bean.setDataSource(dataSource);
        //添加CAT插件
//        CatMybatisPlugin.addCatMybatisPlugin(bean);
//        CatMybatisInterceptor.addCatMybatisInterceptor(bean, jdbcUrl);
        return bean.getObject();
    }

    @Bean(name = "edusohoTransactionManager")
    public DataSourceTransactionManager testTransactionManager
            (@Qualifier("edusohoDataSource") DataSource dataSource) {
        return new DataSourceTransactionManager(dataSource);
    }

    @Bean(name = "edusohoSqlSessionTemplate")
    public SqlSessionTemplate testSqlSessionTemplate
            (@Qualifier("edusohoSqlSessionFactory") SqlSessionFactory sqlSessionFactory) throws Exception {
        return new SqlSessionTemplate(sqlSessionFactory);
    }
}
