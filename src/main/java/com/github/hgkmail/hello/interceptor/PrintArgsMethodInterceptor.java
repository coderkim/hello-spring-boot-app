package com.github.hgkmail.hello.interceptor;

import com.github.hgkmail.hello.dao.ry.SysMenuDao;
import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Spring Proxy，用来替换bean
 */
public class PrintArgsMethodInterceptor implements MethodInterceptor {

    private static final Logger logger = LoggerFactory.getLogger(PrintArgsMethodInterceptor.class);

    @Override
    public Object invoke(MethodInvocation invocation) throws Throwable {
        Object ret = invocation.proceed();
        if (invocation.getMethod().getDeclaringClass().equals(SysMenuDao.class)) {
            logger.info("Print Args: {} {}", invocation.getMethod().getName(), invocation.getArguments());
        }
        return ret;
    }
}
