package com.github.hgkmail.hello.entity.generate;

import com.github.hgkmail.hello.cmd.helper.IndexKey;
import com.github.hgkmail.hello.cmd.helper.NormalIndex;
import com.github.hgkmail.hello.cmd.helper.UniqueIndex;

import java.util.Date;

/**
 * table 用户驻房统计
 *
 * @author kimhuang
 * @date 2021/11/15
 *
 * @table t_room_stay_summary
 * @comment 用户驻房统计
 * @charset utf8
 */
public abstract class RoomStaySummary {

    ///fields///

    /**
     * @primary
     * @dbType bigint(20) unsigned
     * @dbAttr NOT NULL, AUTO_INCREMENT
     * @comment 自增id
     */
    Long id;

    /**
     * @dbType bigint(20)
     * @comment 主播酷狗id
     * @dbDefault '0'
     * @default 0
     * @cond
     * @transfer Long starKugouId 主播酷狗id
     */
    Long starKugouId;

    /**
     * @dbType int(10)
     * @comment 房间号
     * @dbDefault '0'
     * @default 0
     * @transfer Long starKugouId 主播酷狗id
     */
    Long roomId;

    /**
     * @dbType int(10)
     * @comment 开始时间，单位秒
     * @dbDefault '0'
     * @default 0
     * @transfer Long startTime 开始时间，单位：秒
     */
    Long startTime;

    /**
     * @dbType int(10)
     * @comment 结束时间，单位秒
     * @dbDefault '0'
     * @default 0
     * @transfer Long endTime 结束时间，单位：秒
     * @condRange resultStart, resultTimeEnd
     * @condType Long
     */
    Long endTime;

    /**
     * @dbType int(10)
     * @comment 统计时长，单位：秒
     * @dbDefault '0'
     * @default 0
     * @transfer Long duration 统计时长，单位：秒
     */
    Long duration;

    /**
     * @dbType tinyint(1)
     * @dbDefault '0'
     * @default 0
     * @cond
     * @comment 是否来自航母，0:否, 1:是
     * @enum 0:来自独立APP:no, 1:来自航母:yes
     * @transfer Integer fromCarrier 是否来自航母
     */
    Integer fromCarrier;

    /**
     * @dbType varchar(200)
     * @dbDefault ''
     * @default ""
     * @comment 海报地址
     * @transfer String coverUrl 海报完整地址
     */
    String coverUrl;

    /**
     * @dbType varchar(200)
     * @dbDefault ''
     * @default ""
     * @comment 直播间截图地址
     * @transfer String screenshotUrl 直播间截图完整地址
     */
    String screenshotUrl;

    /**
     * @dbType int(10)
     * @comment 驻房流水总数
     * @dbDefault '0'
     * @default 0
     */
    Long logTotalNum;

    /**
     * @dbType int(10)
     * @comment 闪退率不达标的驻房流水数量
     * @dbDefault '0'
     * @default 0
     */
    Long logFailNum;

    /**
     * @dbType int(10)
     * @comment 闪退率百分比
     * @dbDefault '0'
     * @default 0
     */
    Integer flashExitPercent;

    /**
     * @dbType int(10)
     * @comment 闪退率达标门槛，百分比格式
     * @dbDefault '0'
     * @default 0
     */
    Integer flashExitPercentStd;

    /**
     * @dbType tinyint(1)
     * @dbDefault '0'
     * @default 0
     * @cond
     * @comment 观众闪退率状态，0-统计中 1-达标 2-未达标 3-无效
     * @enum 0:统计中:DOING, 1:达标:SUCCESS, 2:未达标:FAIL, 3:无效:INVALID
     */
    Integer status;

    /**
     * @dbType tinyint(1)
     * @dbDefault '0'
     * @default 0
     * @comment 是否已弹窗，0:否, 1:是
     * @enum 0:未弹窗:no, 1:已弹窗:yes
     */
    Integer hasPopup;

    /**
     * @dbType datetime
     * @comment 创建时间
     * @dbAttr NOT NULL DEFAULT CURRENT_TIMESTAMP
     */
    Date createTime;

    /**
     * @dbType datetime
     * @comment 更新时间
     * @dbAttr NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
     */
    Date updateTime;

    ///indexes///

    abstract UniqueIndex coverUrl_fromCarrier(IndexKey coverUrl, IndexKey fromCarrier);

    abstract NormalIndex roomId(IndexKey roomId);

    abstract NormalIndex duration(IndexKey duration);

    abstract NormalIndex status(IndexKey status);

}
