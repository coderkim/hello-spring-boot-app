package com.github.hgkmail.hello.entity.generate;

import com.github.hgkmail.hello.cmd.helper.IndexKey;
import com.github.hgkmail.hello.cmd.helper.NormalIndex;

import java.math.BigDecimal;
import java.util.Date;

/**
 * table 对公率表
 *
 * @author kimhuang
 * @date 2022/7/29
 *
 * @table t_star_settle_by_clan_percent
 * @comment 对公率表
 * @charset utf8
 */
public abstract class StarSettleByClanPercent {

    ///fields///

    /**
     * @dbType int(10) unsigned
     * @dbAttr NOT NULL
     * @comment 公会id
     * @cond chooseClanId
     * @condType Integer
     * @transfer Integer clanId 公会id
     */
    Integer clanId;

    /**
     * @dbType int(10)
     * @comment 年月日，格式YYYYMMDD
     * @dbDefault '0'
     * @cond chooseDay
     * @condType Integer
     */
    Integer day;

    /**
     * @dbType int(10)
     * @comment 年月
     * @dbDefault '0'
     * @cond chooseYearMonth
     * @condType Integer
     */
    Integer yearMonth;

    /**
     * @dbType decimal(5,3)
     * @dbAttr NOT NULL
     * @comment 对公率
     * @transfer
     */
    BigDecimal percent;

    /**
     * @dbType datetime
     * @comment 创建时间
     * @dbAttr NOT NULL
     * @view String createTimeStr 添加时间 DateUtil.format($,DateUtil.YYYY_MM_DD_TIME)
     * @transfer Long createTimestamp 创建时间戳 DateUtil.getUnixTimestamp($)
     */
    Date createTime;

    ///indexes///

    abstract NormalIndex clanId(IndexKey clanId);

    abstract NormalIndex day(IndexKey day);

    abstract NormalIndex yearMonth(IndexKey yearMonth);

}
