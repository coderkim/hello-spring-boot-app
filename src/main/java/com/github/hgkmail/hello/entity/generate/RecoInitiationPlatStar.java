package com.github.hgkmail.hello.entity.generate;

import com.github.hgkmail.hello.cmd.helper.IndexKey;
import com.github.hgkmail.hello.cmd.helper.NormalIndex;
import com.github.hgkmail.hello.cmd.helper.UniqueIndex;

import java.util.Date;

/**
 * table 推荐入会平台主播数据
 *
 * @author kimhuang
 * @date 2022/11/25
 *
 * @table t_recommend_initiation_plat_star
 * @comment 推荐入会平台主播数据
 * @charset utf8
 */
public abstract class RecoInitiationPlatStar {

    ///fields///

    /**
     * @primary
     * @dbType bigint(20) unsigned
     * @dbAttr NOT NULL, AUTO_INCREMENT
     * @comment 主键
     * @useGenKey
     * @transfer Long id id
     */
    Long id;

    /**
     * @dbType int(10)
     * @comment 日期，格式yyyymmdd
     * @dbDefault '0'
     * @cond chooseDay
     * @condType Integer
     */
    Integer day;

    /**
     * @dbType bigint(20)
     * @comment 主播酷狗id
     * @dbDefault '0'
     * @cond chooseKugouId
     * @condType Long
     */
    Long kugouId;

    /**
     * @dbType tinyint(3)
     * @dbDefault '0'
     * @default 0
     * @cond
     * @comment 微博状态，0:未知, 1:平台新主播, 2:流失主播
     * @enum 0:未知:unknown, 1:平台新主播:fresh, 2:流失主播:run
     */
    Integer type;

    /**
     * @dbType int(10)
     * @comment 明星等级
     * @dbDefault '0'
     * @cond chooseStarLevel
     * @condType Integer
     */
    Integer starLevel;

    /**
     * @dbType datetime
     * @comment 主播新签日期
     * @dbAttr DEFAULT NULL
     * @condRange signTimeStart, signTimeEnd
     * @condType String
     * @view String signTimeStr 添加时间 DateUtil.format($,DateUtil.YYYY_MM_DD_TIME)
     * @transfer Long signTimestamp 创建时间戳 DateUtil.getUnixTimestamp($)
     */
    Date signTime;

    /**
     * @dbType datetime
     * @comment 创建时间
     * @dbAttr DEFAULT NULL
     * @condRange createTimeStart, createTimeEnd
     * @condType String
     * @view String createTimeStr 添加时间 DateUtil.format($,DateUtil.YYYY_MM_DD_TIME)
     * @transfer Long createTimestamp 创建时间戳 DateUtil.getUnixTimestamp($)
     */
    Date createTime;

    /**
     * @dbType datetime
     * @comment 更新时间
     * @dbDefault DEFAULT NULL
     */
    Date updateTime;

    ///indexes///

    abstract NormalIndex day(IndexKey day);

}
