package com.github.hgkmail.hello.entity.generate;

import com.github.hgkmail.hello.cmd.helper.IndexKey;
import com.github.hgkmail.hello.cmd.helper.NormalIndex;
import com.github.hgkmail.hello.cmd.helper.UniqueIndex;

import java.util.Date;

/**
 * table 资源转存记录表
 *
 * @author kimhuang
 * @date 2022/12/12
 *
 * @table t_shot_transfer_resource_log
 * @comment 资源转存记录表
 * @charset utf8
 */
public abstract class ShotTransferResourceLog {

    ///fields///

    /**
     * @primary
     * @dbType bigint(20) unsigned
     * @dbAttr NOT NULL, AUTO_INCREMENT
     * @comment 主键
     * @useGenKey
     * @transfer Long id id
     */
    Long id;

    /**
     * @dbType bigint(20)
     * @comment 主播酷狗id
     * @dbDefault '0'
     * @cond chooseKugouId
     * @condType Long
     */
    Long kugouId;

    /**
     * @dbType bigint(20)
     * @comment 截图时间
     * @dbDefault '0'
     * @cond chooseShotTime
     * @condType Long
     */
    Long shotTime;

    /**
     * @dbType varchar(1024)
     * @comment 原url
     * @dbDefault ''
     * @default ""
     * @view _ _ 原url
     * @transfer _ _ 原url
     */
    String originUrl;

    /**
     * @dbType varchar(1024)
     * @comment 新url
     * @dbDefault ''
     * @default ""
     * @view _ _ 新url
     * @transfer _ _ 新url
     */
    String newUrl;

    /**
     * @dbType tinyint(1)
     * @dbDefault '0'
     * @default 0
     * @cond
     * @comment 是否更新t_shot_picinfo表的url，0:否, 1:是
     * @enum 0:否:no, 1:是:yes
     */
    Integer updatePicInfo;

    /**
     * @dbType datetime
     * @comment 创建时间
     * @dbAttr NOT NULL
     * @condRange createTimeStart, createTimeEnd
     * @condType String
     * @view String createTimeStr 添加时间 DateUtil.format($,DateUtil.YYYY_MM_DD_TIME)
     * @transfer Long createTimestamp 创建时间戳 DateUtil.getUnixTimestamp($)
     */
    Date createTime;

    ///indexes///

    abstract UniqueIndex kugouId_shotTime(IndexKey kugouId, IndexKey shotTime);

}
