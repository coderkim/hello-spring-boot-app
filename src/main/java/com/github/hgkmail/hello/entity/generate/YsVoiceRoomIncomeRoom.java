package com.github.hgkmail.hello.entity.generate;

import com.github.hgkmail.hello.cmd.helper.IndexKey;
import com.github.hgkmail.hello.cmd.helper.NormalIndex;

import java.math.BigDecimal;
import java.util.Date;

/**
 * table 鱼声语音房房间收入统计
 *
 * @author kimhuang
 * @date 2022/06/06
 *
 * @table t_ys_voice_room_income_room
 * @comment 鱼声语音房房间收入统计
 * @charset utf8
 */
public abstract class YsVoiceRoomIncomeRoom {

    ///fields///

    /**
     * @primary
     * @dbType bigint(20) unsigned
     * @dbAttr NOT NULL, AUTO_INCREMENT
     * @comment 自增id
     * @useGenKey
     * @transfer Long id 自增id
     */
    Long id;

    /**
     * @dbType int(10)
     * @comment 公会id
     * @dbDefault '0'
     * @cond chooseClanId
     * @condType Long
     */
    Long clanId;

    /**
     * @dbType int(10)
     * @comment 数据日期
     * @dbDefault '0'
     * @condRange dayBegin, dayEnd
     * @condType Integer
     */
    Integer day;

    /**
     * @dbType bigint(20)
     * @comment 房间id
     * @dbDefault '0'
     * @cond chooseRoomId
     * @condType Long
     */
    Long roomId;

    /**
     * @dbType bigint(20)
     * @comment 房主酷狗id
     * @dbDefault '0'
     * @cond chooseOwnerKugouId
     * @condType Long
     */
    Long ownerKugouId;

    /**
     * @dbType bigint(20)
     * @comment 房主繁星id
     * @dbDefault '0'
     * @cond chooseOwnerUserId
     * @condType Long
     */
    Long ownerUserId;

    /**
     * @dbType varchar(200)
     * @dbDefault ''
     * @comment 房主昵称
     * @default ""
     * @view _ nickname 房主昵称
     * @transfer _ _ 房主昵称
     */
    String ownerNickname;

    /**
     * @dbType int(10)
     * @comment 开播时长
     * @dbDefault '0'
     */
    Integer liveTime;

    /**
     * @dbType decimal(32,2)
     * @dbAttr NOT NULL
     * @comment 收礼流水，元
     * @transfer
     */
    BigDecimal totalCoin;

    /**
     * @dbType decimal(32,2)
     * @dbAttr NOT NULL
     * @comment 充值账户流水，元
     * @transfer
     */
    BigDecimal rechargeCoin;

    /**
     * @dbType decimal(32,2)
     * @dbAttr NOT NULL
     * @comment 仓库收礼流水，元
     * @transfer
     */
    BigDecimal warehouseCoin;

    /**
     * @dbType decimal(32,2)
     * @dbAttr NOT NULL
     * @comment 仓库礼物占比
     * @transfer
     */
    BigDecimal warehouseRate;

    /**
     * @dbType decimal(32,2)
     * @dbAttr NOT NULL
     * @comment 其他收礼流水，元
     * @transfer
     */
    BigDecimal otherCoin;

    /**
     * @dbType timestamp
     * @comment 添加时间
     * @dbAttr NOT NULL
     * @condRange createTimeStart, createTimeEnd
     * @condType String
     * @view String createTimeStr 添加时间 DateUtil.format($,DateUtil.YYYY_MM_DD_TIME)
     * @transfer Long createTimestamp 创建时间戳 DateUtil.getUnixTimestamp($)
     */
    Date createTime;

    ///indexes///

    abstract NormalIndex clanId(IndexKey clanId);

    abstract NormalIndex day(IndexKey day);

    abstract NormalIndex roomId(IndexKey roomId);

    abstract NormalIndex ownerKugouId(IndexKey ownerKugouId);

}
