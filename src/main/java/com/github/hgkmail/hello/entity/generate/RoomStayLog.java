package com.github.hgkmail.hello.entity.generate;

import com.github.hgkmail.hello.cmd.helper.IndexKey;
import com.github.hgkmail.hello.cmd.helper.NormalIndex;

import java.util.Date;

/**
 * table 用户驻房流水
 *
 * @author kimhuang
 * @date 2021/11/09
 *
 * @table t_room_stay_log_yyyyMM
 * @comment 用户驻房流水表
 * @charset utf8
 */
public abstract class RoomStayLog {

    ///fields///

    /**
     * @primary
     * @dbType bigint(20) unsigned
     * @dbAttr NOT NULL, AUTO_INCREMENT
     * @comment 自增id
     */
    Long id;

    /**
     * @dbType bigint(20)
     * @comment 观众酷狗id
     * @dbDefault '0'
     * @default 0
     * @transfer Long userKugouId 用户酷狗id，未登录用户不用填
     */
    Long userKugouId;

    /**
     * @dbType varchar(200)
     * @dbDefault ''
     * @default ""
     * @comment 设备id
     * @transfer String deviceId 设备id
     */
    String deviceId;

    /**
     * @dbType int(10)
     * @comment 持续时间，单位：毫秒
     * @dbDefault '0'
     * @default 0
     * @transfer Long stayDuration 持续时间，单位：毫秒
     */
    Long duration;

    /**
     * @dbType tinyint(1)
     * @dbDefault '0'
     * @default 0
     * @cond
     * @comment 是否来自航母，0:否, 1:是
     * @enum 0:来自独立APP:no, 1:来自航母:yes
     * @transfer Integer fromCarrier 是否来自航母
     */
    Integer fromCarrier;

    /**
     * @dbType int(10)
     * @comment 房间号
     * @dbDefault '0'
     * @default 0
     * @transfer Long starKugouId 主播酷狗id
     */
    Long roomId;

    /**
     * @dbType varchar(200)
     * @dbDefault ''
     * @default ""
     * @comment 海报地址
     * @transfer String coverUrl 海报完整地址
     */
    String coverUrl;

    /**
     * @dbType varchar(200)
     * @dbDefault ''
     * @default ""
     * @comment 直播间截图地址
     * @transfer String screenshotUrl 直播间截图完整地址
     */
    String screenshotUrl;

    /**
     * @dbType datetime
     * @comment 创建时间
     * @dbAttr NOT NULL DEFAULT CURRENT_TIMESTAMP
     * @condRange createTimeStart, createTimeEnd
     * @condType String
     */
    Date createTime;

    ///indexes///

    abstract NormalIndex roomId(IndexKey roomId);

    abstract NormalIndex cover(IndexKey coverUrl);

    abstract NormalIndex duration(IndexKey duration);

}
