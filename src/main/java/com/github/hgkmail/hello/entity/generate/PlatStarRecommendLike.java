package com.github.hgkmail.hello.entity.generate;

import com.github.hgkmail.hello.cmd.helper.IndexKey;
import com.github.hgkmail.hello.cmd.helper.NormalIndex;
import com.github.hgkmail.hello.cmd.helper.UniqueIndex;

import java.util.Date;

/**
 * table 平台主播推荐入会-喜欢的主播
 *
 * @author kimhuang
 * @date 2022/11/28
 *
 * @table t_plat_star_recommend_like
 * @comment 平台主播推荐入会-喜欢的主播
 * @charset utf8
 */
public abstract class PlatStarRecommendLike {

    ///fields///

    /**
     * @primary
     * @dbType bigint(20) unsigned
     * @dbAttr NOT NULL, AUTO_INCREMENT
     * @comment 主键
     * @useGenKey
     * @transfer Long id id
     */
    Long id;

    /**
     * @dbType int(10) unsigned
     * @comment 公会id
     * @dbDefault '0'
     * @cond chooseClanId
     * @condType Integer
     */
    Integer clanId;

    /**
     * @dbType bigint(20) unsigned
     * @comment 管理员id
     * @dbDefault '0'
     * @cond chooseAdminId
     * @condType Long
     */
    Long adminId;

    /**
     * @dbType bigint(20) unsigned
     * @comment 主播酷狗id
     * @dbDefault '0'
     * @cond chooseStarKugouId
     * @condType Long
     */
    Long starKugouId;

    /**
     * @dbType datetime
     * @comment 创建时间
     * @dbAttr DEFAULT NULL
     * @condRange createTimeStart, createTimeEnd
     * @condType String
     * @view String createTimeStr 添加时间 DateUtil.format($,DateUtil.YYYY_MM_DD_TIME)
     * @transfer Long createTimestamp 创建时间戳 DateUtil.getUnixTimestamp($)
     */
    Date createTime;

    ///indexes///

    abstract UniqueIndex clanId_starKugouId(IndexKey clanId, IndexKey starKugouId);

    abstract NormalIndex adminId(IndexKey adminId);

}
