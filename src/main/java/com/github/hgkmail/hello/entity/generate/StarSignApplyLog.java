package com.github.hgkmail.hello.entity.generate;

import com.github.hgkmail.hello.cmd.helper.IndexKey;
import com.github.hgkmail.hello.cmd.helper.NormalIndex;

/**
 * table 入驻申请日志表
 *
 * @author kimhuang
 * @date 2022/02/28
 *
 * @table t_star_sign_apply_log
 * @comment 入驻申请日志表
 * @charset utf8
 */
public abstract class StarSignApplyLog {

    ///fields///

    /**
     * @primary
     * @dbType int(10) unsigned
     * @dbAttr NOT NULL AUTO_INCREMENT
     * @comment 自增id
     * @transfer
     */
    Long id;

    /**
     * @dbType int(10) unsigned
     * @dbAttr NOT NULL
     * @comment 申请id
     * @transfer
     */
    Long applyId;

    /**
     * @dbType tinyint(4) unsigned
     * @dbAttr NOT NULL
     * @comment 状态
     * @transfer
     */
    Integer status;

    /**
     * @dbType int(10) unsigned
     * @dbAttr NOT NULL
     * @comment 添加时间
     * @transfer
     */
    Integer addTime;

    /**
     * @dbType int(10) unsigned
     * @dbAttr NOT NULL
     * @comment 操作者id
     * @transfer
     */
    Integer operatorId;

    ///indexes///

    abstract NormalIndex applyId(IndexKey applyId, IndexKey status);

}
