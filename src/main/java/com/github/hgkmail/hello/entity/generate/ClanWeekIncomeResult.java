package com.github.hgkmail.hello.entity.generate;

import com.github.hgkmail.hello.cmd.helper.IndexKey;
import com.github.hgkmail.hello.cmd.helper.NormalIndex;
import com.github.hgkmail.hello.cmd.helper.UniqueIndex;

import java.math.BigDecimal;
import java.util.Date;

/**
 * table 公会收益周任务结果表
 *
 * @author kimhuang
 * @date 2021/12/12
 *
 * @table t_family_biz_clan_week_income_result
 * @comment 公会收益周任务结果表
 * @charset utf8
 */
public abstract class ClanWeekIncomeResult {

    ///fields///

    /**
     * @primary
     * @dbType bigint(20) unsigned
     * @dbAttr NOT NULL
     * @comment 全局id
     * @transfer
     */
    Long id;

    /**
     * @dbType bigint(20) unsigned
     * @dbAttr NOT NULL
     * @comment 公会id
     * @transfer
     */
    Long clanId;

    /**
     * @dbType int(10) unsigned
     * @dbAttr NOT NULL
     * @comment 年
     * @transfer
     */
    Integer year;

    /**
     * @dbType int(10) unsigned
     * @dbAttr NOT NULL
     * @comment 周
     * @transfer
     */
    Integer week;

    /**
     * @dbType int(10) unsigned
     * @dbAttr NOT NULL
     * @comment 周一的日期
     * @transfer
     */
    Integer mondayYmd;

    /**
     * @dbType decimal(32,2)
     * @dbAttr NOT NULL
     * @comment 实际星豆数
     * @transfer
     */
    BigDecimal resultBean;

    /**
     * @dbType int(10) unsigned
     * @dbAttr NOT NULL
     * @comment 达到档位（0、1、2、3）
     * @transfer
     */
    Integer resultLevel;

    /**
     * @dbType decimal(32,2)
     * @dbAttr NOT NULL
     * @comment 奖励系数
     * @transfer
     */
    BigDecimal awardRate;

    /**
     * @dbType decimal(32,2)
     * @dbAttr NOT NULL
     * @comment 奖励星豆数（未考虑封顶）
     * @transfer
     */
    BigDecimal awardBean;

    /**
     * @dbType decimal(32,2)
     * @dbAttr NOT NULL
     * @comment 奖励人民币封顶值
     * @transfer
     */
    BigDecimal maxAwardRmb;

    /**
     * @dbType decimal(32,2)
     * @dbAttr NOT NULL
     * @comment 实际奖励人民币（结算）
     * @transfer
     */
    BigDecimal actualAwardRmb;

    /**
     * @dbType timestamp
     * @comment 创建时间
     * @dbAttr NOT NULL DEFAULT CURRENT_TIMESTAMP
     * @transfer
     */
    Date createTime;

    /**
     * @dbType timestamp
     * @comment 更新时间
     * @dbAttr NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
     * @transfer
     */
    Date updateTime;

    ///indexes///

    abstract UniqueIndex uniq_clanId_year_week(IndexKey clanId, IndexKey year, IndexKey week);

    abstract NormalIndex idx_year_week(IndexKey year, IndexKey week);

}
