package com.github.hgkmail.hello.entity.generate;

import com.github.hgkmail.hello.cmd.helper.IndexKey;
import com.github.hgkmail.hello.cmd.helper.NormalIndex;
import com.github.hgkmail.hello.cmd.helper.UniqueIndex;

import java.util.Date;

/**
 * table 非本人直播监控按天统计表
 *
 * @author kimhuang
 * @date 2022/11/09
 *
 * @table t_live_monitor_warning_day_stats
 * @comment 非本人直播监控按天统计表
 * @charset utf8
 */
public abstract class LiveMonitorWarningDayStats {

    ///fields///

    /**
     * @primary
     * @dbType int(10) unsigned
     * @dbAttr NOT NULL
     * @comment 日期，格式yyyyMMdd
     * @cond chooseDay
     * @condType Integer
     * @transfer Integer day 日期
     */
    Integer day;

    /**
     * @dbType int(10) unsigned
     * @comment 人脸识别量
     * @dbDefault '0'
     * @default 0
     */
    Integer compareNum;

    /**
     * @dbType int(10) unsigned
     * @comment 审核量
     * @dbDefault '0'
     * @default 0
     */
    Integer auditNum;

    /**
     * @dbType int(10) unsigned
     * @comment 人工限播量
     * @dbDefault '0'
     * @default 0
     */
    Integer humanLimitLiveNum;

    /**
     * @dbType int(10) unsigned
     * @comment 系统限播量
     * @dbDefault '0'
     * @default 0
     */
    Integer systemLimitLiveNum;

    /**
     * @dbType int(10) unsigned
     * @comment 人工异常告警量
     * @dbDefault '0'
     * @default 0
     */
    Integer humanWrongWarnNum;

    /**
     * @dbType int(10) unsigned
     * @comment 系统异常告警量
     * @dbDefault '0'
     * @default 0
     */
    Integer systemWrongWarnNum;

    /**
     * @dbType int(10) unsigned
     * @comment 人工忽略量
     * @dbDefault '0'
     * @default 0
     */
    Integer humanIgnoreNum;

    /**
     * @dbType int(10) unsigned
     * @comment 系统忽略量
     * @dbDefault '0'
     * @default 0
     */
    Integer systemIgnoreNum;

    /**
     * @dbType int(10) unsigned
     * @comment 封房量
     * @dbDefault '0'
     * @default 0
     */
    Integer banRoomNum;

    /**
     * @dbType timestamp
     * @comment 创建时间
     * @dbAttr NOT NULL
     * @condRange createTimeStart, createTimeEnd
     * @condType String
     * @view String createTimeStr 添加时间 DateUtil.format($,DateUtil.YYYY_MM_DD_TIME)
     * @transfer Long createTimestamp 创建时间戳 DateUtil.getUnixTimestamp($)
     */
    Date createTime;

    /**
     * @dbType timestamp
     * @comment 更新时间
     * @dbDefault CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
     * @dbTrigger
     */
    Date updateTime;

    ///indexes///

    abstract UniqueIndex day(IndexKey day);

}
