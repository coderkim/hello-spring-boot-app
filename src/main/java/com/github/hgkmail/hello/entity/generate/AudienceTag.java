package com.github.hgkmail.hello.entity.generate;

import com.github.hgkmail.hello.cmd.helper.IndexKey;
import com.github.hgkmail.hello.cmd.helper.NormalIndex;
import com.github.hgkmail.hello.cmd.helper.UniqueIndex;

import java.util.Date;

/**
 * table 直播雷达观众标签
 *
 * @author kimhuang
 * @date 2022/9/12
 *
 * @table t_audience_tag
 * @comment 直播雷达观众标签表
 * @charset utf8
 */
public abstract class AudienceTag {

    ///fields///

    /**
     * @primary
     * @dbType bigint(20) unsigned
     * @dbAttr NOT NULL
     * @comment 全局id
     * @transfer Long id 全局id
     */
    Long id;

    /**
     * @dbType int(10)
     * @comment 日期，格式yyyymmdd
     * @dbDefault '0'
     * @cond chooseDay
     * @condType Integer
     */
    Integer day;

    /**
     * @dbType bigint(20)
     * @comment 主播繁星id
     * @dbDefault '0'
     * @default 0
     * @transfer Long starFxId 主播繁星id
     */
    Long starFxId;

    /**
     * @dbType bigint(20)
     * @comment 观众繁星id
     * @dbDefault '0'
     * @default 0
     * @transfer Long userFxId 观众繁星id
     */
    Long userFxId;

    /**
     * @dbType tinyint(4)
     * @dbDefault '0'
     * @default 0
     * @cond
     * @comment 微博状态，0:无, 1:好久不见, 2:高潜
     * @enum 0:无:none, 1:好久不见:longTimeNoSee, 2:高潜:highPotential
     */
    Integer tag;

    /**
     * @dbType timestamp
     * @comment 创建时间
     * @dbAttr NOT NULL
     * @condRange createTimeStart, createTimeEnd
     * @condType String
     * @view String createTimeStr 添加时间 DateUtil.format($,DateUtil.YYYY_MM_DD_TIME)
     * @transfer Long createTimestamp 创建时间戳 DateUtil.getUnixTimestamp($)
     */
    Date createTime;

    ///indexes///

    abstract NormalIndex day(IndexKey day);

    abstract NormalIndex starFxId_userFxId(IndexKey starFxId, IndexKey userFxId);

}
