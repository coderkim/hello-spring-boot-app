package com.github.hgkmail.hello.entity.generate;

import com.github.hgkmail.hello.cmd.helper.IndexKey;
import com.github.hgkmail.hello.cmd.helper.NormalIndex;

import java.math.BigDecimal;
import java.util.Date;

/**
 * table 公会抽成比例限制表
 *
 * @author kimhuang
 * @date 2022/7/29
 *
 * @table t_sign_clan_star_ratio_limit
 * @comment 公会抽成比例限制表
 * @charset utf8
 */
public abstract class SignClanStarRatioLimit {

    ///fields///

    /**
     * @primary
     * @dbType bigint(20) unsigned
     * @dbAttr NOT NULL
     * @comment 全局id
     * @cond
     * @transfer Long id 全局id
     */
    Long id;

    /**
     * @dbType int(10)
     * @comment 公会id
     * @dbDefault '0'
     * @cond chooseClanId
     * @condType Integer
     */
    Integer clanId;

    /**
     * @dbType varchar(255)
     * @dbDefault ''
     * @comment 公会名称
     * @transfer _ _ 公会名称
     */
    String clanName;

    /**
     * @dbType int(10)
     * @comment 年月
     * @dbDefault '0'
     * @cond chooseYearMonth
     * @condType Integer
     */
    Integer yearMonth;

    /**
     * @dbType tinyint(2)
     * @dbDefault '0'
     * @default 0
     * @cond
     * @comment 状态，0:待调整, 1:已调整, 2:已过期
     * @enum 0:待调整:none, 1:已调整:adjusted, 2:已过期:expired
     */
    Integer status;

    /**
     * @dbType decimal(5,2)
     * @dbAttr NOT NULL
     * @comment 最高抽成比例限制
     * @transfer
     */
    BigDecimal limitRatio;

    /**
     * @dbType int(10)
     * @comment 导入时间戳
     * @dbDefault '0'
     * @transfer
     */
    Integer importTime;

    /**
     * @dbType int(10)
     * @comment 调整生效时间戳
     * @dbDefault '0'
     * @transfer
     */
    Integer effectTime;

    /**
     * @dbType int(10)
     * @comment 调整过期时间，默认次月1号0点
     * @dbDefault '0'
     * @transfer
     */
    Integer expireTime;

    /**
     * @dbType varchar(100)
     * @dbDefault ''
     * @comment 操作人
     * @transfer _ _ 操作人
     */
    String operator;

    /**
     * @dbType datetime
     * @comment 创建时间
     * @dbAttr NOT NULL
     * @view String createTimeStr 添加时间 DateUtil.format($,DateUtil.YYYY_MM_DD_TIME)
     * @transfer Long createTimestamp 创建时间戳 DateUtil.getUnixTimestamp($)
     */
    Date createTime;

    /**
     * @dbType datetime
     * @comment 创建时间
     * @dbAttr NOT NULL
     * @view String updateTimeStr 添加时间 DateUtil.format($,DateUtil.YYYY_MM_DD_TIME)
     * @transfer Long updateTimestamp 创建时间戳 DateUtil.getUnixTimestamp($)
     */
    Date updateTime;

    ///indexes///

    abstract NormalIndex clanId(IndexKey clanId);

    abstract NormalIndex yearMonth(IndexKey yearMonth);

}
