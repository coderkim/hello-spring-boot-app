package com.github.hgkmail.hello.entity.generate;

import com.github.hgkmail.hello.cmd.helper.IndexKey;
import com.github.hgkmail.hello.cmd.helper.NormalIndex;
import com.github.hgkmail.hello.cmd.helper.UniqueIndex;

import java.math.BigDecimal;
import java.util.Date;

/**
 * table 公会收益周任务目标表
 *
 * @author kimhuang
 * @date 2021/12/12
 *
 * @table t_family_biz_clan_week_income_target
 * @comment 公会收益周任务目标表
 * @charset utf8
 */
public abstract class ClanWeekIncomeTarget {

    ///fields///

    /**
     * @primary
     * @dbType bigint(20) unsigned
     * @dbAttr NOT NULL
     * @comment 全局id
     * @transfer
     */
    Long id;

    /**
     * @dbType bigint(20) unsigned
     * @dbAttr NOT NULL
     * @comment 公会id
     * @transfer
     */
    Long clanId;

    /**
     * @dbType int(10) unsigned
     * @dbAttr NOT NULL
     * @comment 年
     * @transfer
     */
    Integer year;

    /**
     * @dbType int(10) unsigned
     * @dbAttr NOT NULL
     * @comment 周
     * @transfer
     */
    Integer week;

    /**
     * @dbType int(10) unsigned
     * @dbAttr NOT NULL
     * @comment 周一的日期
     * @transfer
     */
    Integer mondayYmd;

    /**
     * @dbType decimal(32,2)
     * @dbAttr NOT NULL
     * @comment 公会周均流水
     * @transfer
     */
    BigDecimal avgConsumeMoneyCoin;

    /**
     * @dbType decimal(32,2)
     * @dbAttr NOT NULL
     * @comment 公会周均星豆(基准值)
     * @transfer
     */
    BigDecimal baseBean;

    /**
     * @dbType text
     * @default ""
     * @comment 周均星豆目标值规则(json格式)
     * @transfer
     */
    String targetRule;

    /**
     * @dbType timestamp
     * @comment 创建时间
     * @dbAttr NOT NULL DEFAULT CURRENT_TIMESTAMP
     * @transfer
     */
    Date createTime;

    ///indexes///

    abstract UniqueIndex uniq_clanId_year_week(IndexKey clanId, IndexKey year, IndexKey week);

    abstract NormalIndex idx_year_week(IndexKey year, IndexKey week);

}
