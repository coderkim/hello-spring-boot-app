package com.github.hgkmail.hello.entity.generate;

import com.github.hgkmail.hello.cmd.helper.IndexKey;
import com.github.hgkmail.hello.cmd.helper.NormalIndex;

import java.math.BigDecimal;
import java.util.Date;

/**
 * table 鱼声语音房公会收入统计
 *
 * @author kimhuang
 * @date 2022/06/06
 *
 * @table t_ys_voice_room_income_clan
 * @comment 鱼声语音房公会收入统计
 * @charset utf8
 */
public abstract class YsVoiceRoomIncomeClan {

    ///fields///

    /**
     * @primary
     * @dbType bigint(20) unsigned
     * @dbAttr NOT NULL, AUTO_INCREMENT
     * @comment 自增id
     * @useGenKey
     * @transfer Long id 自增id
     */
    Long id;

    /**
     * @dbType int(10)
     * @comment 公会id
     * @dbDefault '0'
     * @cond chooseClanId
     * @condType Long
     */
    Long clanId;

    /**
     * @dbType int(10)
     * @comment 数据日期
     * @dbDefault '0'
     * @condRange dayBegin, dayEnd
     * @condType Integer
     */
    Integer day;

    /**
     * @dbType int(10)
     * @comment 开播时长
     * @dbDefault '0'
     */
    Integer liveTime;

    /**
     * @dbType int(10)
     * @comment 开播主播数
     * @dbDefault '0'
     */
    Integer liveStarNum;

    /**
     * @dbType int(10)
     * @comment 活跃主播数
     * @dbDefault '0'
     */
    Integer activeStarNum;

    /**
     * @dbType decimal(32,2)
     * @dbAttr NOT NULL
     * @comment 抽成前星豆收入
     * @transfer
     */
    BigDecimal beanBeforeShare;

    /**
     * @dbType decimal(32,2)
     * @dbAttr NOT NULL
     * @comment 抽成后星豆收入
     * @transfer
     */
    BigDecimal beanAfterShare;

    /**
     * @dbType decimal(32,2)
     * @dbAttr NOT NULL
     * @comment 收礼流水，元
     * @transfer
     */
    BigDecimal totalCoin;

    /**
     * @dbType decimal(32,2)
     * @dbAttr NOT NULL
     * @comment 充值账户流水，元
     * @transfer
     */
    BigDecimal rechargeCoin;

    /**
     * @dbType decimal(32,2)
     * @dbAttr NOT NULL
     * @comment 仓库收礼流水，元
     * @transfer
     */
    BigDecimal warehouseCoin;

    /**
     * @dbType decimal(32,2)
     * @dbAttr NOT NULL
     * @comment 仓库礼物占比
     * @transfer
     */
    BigDecimal warehouseRate;

    /**
     * @dbType decimal(32,2)
     * @dbAttr NOT NULL
     * @comment 其他收礼流水，元
     * @transfer
     */
    BigDecimal otherCoin;

    /**
     * @dbType timestamp
     * @comment 添加时间
     * @dbAttr NOT NULL
     * @view String createTimeStr 添加时间 DateUtil.format($,DateUtil.YYYY_MM_DD_TIME)
     * @transfer Long createTimestamp 创建时间戳 DateUtil.getUnixTimestamp($)
     */
    Date createTime;

    ///indexes///

    abstract NormalIndex clanId(IndexKey clanId);

    abstract NormalIndex day(IndexKey day);

}
