package com.github.hgkmail.hello.entity.generate;

import com.github.hgkmail.hello.cmd.helper.IndexKey;
import com.github.hgkmail.hello.cmd.helper.NormalIndex;

import java.util.Date;

/**
 * table 审丑房名单操作日志
 *
 * @author kimhuang
 * @date 2022/6/29
 *
 * @table t_ugly_room_list_log
 * @comment 审丑房名单操作日志
 * @charset utf8
 */
public abstract class UglyRoomLog {

    ///fields///

    /**
     * @primary
     * @dbType bigint(20) unsigned
     * @dbAttr NOT NULL
     * @comment 全局id
     * @transfer Long id 全局id
     */
    Long id;

    /**
     * @dbType bigint(20) unsigned
     * @dbAttr NOT NULL
     * @comment 房间号
     * @cond
     * @transfer Long roomId 房间号
     */
    Long roomId;

    /**
     * @dbType varchar(255)
     * @dbDefault ''
     * @comment 备注
     * @transfer _ _ 备注
     */
    String remark;

    /**
     * @dbType tinyint(1)
     * @dbDefault '0'
     * @default 0
     * @cond
     * @comment 状态，0:无, 1:添加, 2:删除, 3:更新
     * @enum 0:无:none, 1:添加:add, 2:删除:delete, 3:更新:update
     */
    Integer actionType;

    /**
     * @dbType varchar(100)
     * @dbDefault ''
     * @comment 操作人
     * @transfer _ _ 操作人
     */
    String operator;

    /**
     * @dbType datetime
     * @comment 创建时间
     * @dbAttr NOT NULL
     * @condRange createTimeStart, createTimeEnd
     * @condType String
     * @view String createTimeStr 添加时间 DateUtil.format($,DateUtil.YYYY_MM_DD_TIME)
     * @transfer Long createTimestamp 创建时间戳 DateUtil.getUnixTimestamp($)
     */
    Date createTime;

    ///indexes///

    abstract NormalIndex createTime(IndexKey createTime);

}
