package com.github.hgkmail.hello.entity.generate;

import com.github.hgkmail.hello.cmd.helper.IndexKey;
import com.github.hgkmail.hello.cmd.helper.NormalIndex;

import java.math.BigDecimal;
import java.util.Date;

/**
 * table 鱼声语音房房间收入明细
 *
 * @author kimhuang
 * @date 2022/06/06
 *
 * @table t_ys_voice_room_income_room_detail
 * @comment 鱼声语音房房间收入明细
 * @charset utf8
 */
public abstract class YsVoiceRoomIncomeRoomDetail {

    ///fields///

    /**
     * @primary
     * @dbType bigint(20) unsigned
     * @dbAttr NOT NULL, AUTO_INCREMENT
     * @comment 自增id
     * @useGenKey
     * @transfer Long id 自增id
     */
    Long id;

    /**
     * @dbType int(10)
     * @comment 公会id
     * @dbDefault '0'
     * @cond chooseClanId
     * @condType Long
     */
    Long clanId;

    /**
     * @dbType int(10)
     * @comment 数据日期
     * @dbDefault '0'
     * @cond chooseDay
     * @condType Long
     */
    Integer day;

    /**
     * @dbType bigint(20)
     * @comment 房间id
     * @dbDefault '0'
     * @cond chooseRoomId
     * @condType Long
     */
    Long roomId;

    /**
     * @dbType bigint(20)
     * @comment 收礼主播酷狗id
     * @dbDefault '0'
     * @cond chooseOwnerKugouId
     * @condType Long
     */
    Long receiverKugouId;

    /**
     * @dbType bigint(20)
     * @comment 收礼主播繁星id
     * @dbDefault '0'
     * @cond chooseOwnerUserId
     * @condType Long
     */
    Long receiverUserId;

    /**
     * @dbType varchar(200)
     * @dbDefault ''
     * @comment 收礼主播昵称
     * @default ""
     * @view _ nickname 收礼主播昵称
     * @transfer _ _ 收礼主播昵称
     */
    String receiverNickname;

    /**
     * @dbType decimal(32,2)
     * @dbAttr NOT NULL
     * @comment 收礼流水，元
     * @transfer
     */
    BigDecimal totalCoin;

    /**
     * @dbType decimal(32,2)
     * @dbAttr NOT NULL
     * @comment 充值账户流水，元
     * @transfer
     */
    BigDecimal rechargeCoin;

    /**
     * @dbType decimal(32,2)
     * @dbAttr NOT NULL
     * @comment 仓库收礼流水，元
     * @transfer
     */
    BigDecimal warehouseCoin;

    /**
     * @dbType decimal(32,2)
     * @dbAttr NOT NULL
     * @comment 仓库礼物占比
     * @transfer
     */
    BigDecimal warehouseRate;

    /**
     * @dbType decimal(32,2)
     * @dbAttr NOT NULL
     * @comment 其他收礼流水，元
     * @transfer
     */
    BigDecimal otherCoin;

    /**
     * @dbType timestamp
     * @comment 添加时间
     * @dbAttr NOT NULL
     * @condRange createTimeStart, createTimeEnd
     * @condType String
     * @view String createTimeStr 添加时间 DateUtil.format($,DateUtil.YYYY_MM_DD_TIME)
     * @transfer Long createTimestamp 创建时间戳 DateUtil.getUnixTimestamp($)
     */
    Date createTime;

    ///indexes///

    abstract NormalIndex clanId(IndexKey clanId);

    abstract NormalIndex day(IndexKey day);

    abstract NormalIndex roomId(IndexKey roomId);

    abstract NormalIndex receiverKugouId(IndexKey receiverKugouId);

}
