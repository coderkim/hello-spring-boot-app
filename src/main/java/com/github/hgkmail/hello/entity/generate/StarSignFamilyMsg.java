package com.github.hgkmail.hello.entity.generate;

import com.github.hgkmail.hello.cmd.helper.IndexKey;
import com.github.hgkmail.hello.cmd.helper.NormalIndex;
import com.github.hgkmail.hello.cmd.helper.UniqueIndex;

import java.util.Date;

/**
 * table 主播签约生效时d_family库的持久化消息
 *
 * @author kimhuang
 * @date 2022/03/08
 *
 * @table t_star_sign_family_msg
 * @comment 主播签约生效时d_family库的持久化消息
 * @charset utf8
 */
public abstract class StarSignFamilyMsg {

    ///fields///

    /**
     * @primary
     * @dbType bigint(20) unsigned
     * @dbAttr NOT NULL AUTO_INCREMENT
     * @comment 主键id
     * @transfer
     */
    Long id;

    /**
     * @dbType tinyint(3)
     * @dbAttr NOT NULL
     * @comment 业务类型，1-公会主播签约
     * @enum 1:公会主播签约:clan_star_sign
     * @transfer
     */
    Integer bizType;

    /**
     * @dbType varchar(100)
     * @dbAttr NOT NULL
     * @comment 全局唯一业务id
     * @transfer
     */
    String bizId;

    /**
     * @dbType mediumtext
     * @dbAttr NULL
     * @comment 消息内容
     * @transfer
     */
    String content;

    /**
     * @dbType tinyint(1)
     * @dbAttr NOT NULL
     * @comment 消息结果，0-待发送，1-发送成功，2-发送失败
     * @enum 0:待发送:pending, 1:成功:success, 2:失败:fail
     * @transfer
     */
    Integer result;

    /**
     * @dbType varchar(100)
     * @dbAttr NOT NULL
     * @comment 消息概述
     * @transfer
     */
    String desc;

    /**
     * @dbType tinyint(3) unsigned
     * @dbAttr NOT NULL
     * @comment 重试次数
     * @transfer
     */
    Integer tryCount;

    /**
     * @dbType timestamp
     * @dbAttr NULL
     * @comment 创建时间
     * @transfer
     */
    Date createTime;

    /**
     * @dbType timestamp
     * @dbAttr NULL
     * @comment 更新时间
     * @transfer
     */
    Date updateTime;

    ///indexes///

    abstract UniqueIndex bizId(IndexKey bizId);

    abstract NormalIndex bizType_result(IndexKey bizType, IndexKey result);

}
