package com.github.hgkmail.hello.entity.generate;

import com.github.hgkmail.hello.cmd.helper.IndexKey;
import com.github.hgkmail.hello.cmd.helper.NormalIndex;

import java.math.BigDecimal;
import java.util.Date;

/**
 * table 鱼声语音房主播收入统计
 *
 * @author kimhuang
 * @date 2022/06/06
 *
 * @table t_ys_voice_room_income_star
 * @comment 鱼声语音房主播收入统计
 * @charset utf8
 */
public abstract class YsVoiceRoomIncomeStar {

    ///fields///

    /**
     * @primary
     * @dbType bigint(20) unsigned
     * @dbAttr NOT NULL, AUTO_INCREMENT
     * @comment 自增id
     * @useGenKey
     * @transfer Long id 自增id
     */
    Long id;

    /**
     * @dbType int(10)
     * @comment 公会id
     * @dbDefault '0'
     * @cond chooseClanId
     * @condType Long
     */
    Long clanId;

    /**
     * @dbType int(10)
     * @comment 数据日期
     * @dbDefault '0'
     * @condRange dayBegin, dayEnd
     * @condType Integer
     */
    Integer day;

    /**
     * @dbType bigint(20)
     * @comment 主播酷狗id
     * @dbDefault '0'
     * @cond chooseKugouId
     * @condType Long
     */
    Long starKugouId;

    /**
     * @dbType bigint(20)
     * @comment 主播繁星id
     * @dbDefault '0'
     * @cond chooseUserId
     * @condType Long
     */
    Long starUserId;

    /**
     * @dbType varchar(200)
     * @dbDefault ''
     * @default ""
     * @comment 主播昵称
     * @view _ nickname 主播昵称
     * @transfer _ _ 主播昵称
     */
    String starNickname;

    /**
     * @dbType int(10)
     * @comment 开播时长
     * @dbDefault '0'
     */
    Integer liveTime;

    /**
     * @dbType decimal(32,2)
     * @dbAttr NOT NULL
     * @comment 抽成前的星豆收入
     * @transfer
     */
    BigDecimal beanBeforeShare;

    /**
     * @dbType decimal(32,2)
     * @dbAttr NOT NULL
     * @comment 星豆收入
     * @transfer
     */
    BigDecimal bean;

    /**
     * @dbType decimal(32,2)
     * @dbAttr NOT NULL
     * @comment 收礼流水，元
     * @transfer
     */
    BigDecimal totalCoin;

    /**
     * @dbType decimal(32,2)
     * @dbAttr NOT NULL
     * @comment 充值账户流水，元
     * @transfer
     */
    BigDecimal rechargeCoin;

    /**
     * @dbType decimal(32,2)
     * @dbAttr NOT NULL
     * @comment 仓库收礼流水，元
     * @transfer
     */
    BigDecimal warehouseCoin;

    /**
     * @dbType decimal(32,2)
     * @dbAttr NOT NULL
     * @comment 仓库礼物占比
     * @transfer
     */
    BigDecimal warehouseRate;

    /**
     * @dbType decimal(32,2)
     * @dbAttr NOT NULL
     * @comment 其他收礼流水，元
     * @transfer
     */
    BigDecimal otherCoin;

    /**
     * @dbType timestamp
     * @comment 添加时间
     * @dbAttr NOT NULL
     * @view String createTimeStr 添加时间 DateUtil.format($,DateUtil.YYYY_MM_DD_TIME)
     * @transfer Long createTimestamp 创建时间戳 DateUtil.getUnixTimestamp($)
     */
    Date createTime;

    ///indexes///

    abstract NormalIndex clanId(IndexKey clanId);

    abstract NormalIndex day(IndexKey day);

    abstract NormalIndex starKugouId(IndexKey starKugouId);

}
