package com.github.hgkmail.hello.entity.generate;

import com.github.hgkmail.hello.cmd.helper.IndexKey;
import com.github.hgkmail.hello.cmd.helper.NormalIndex;
import com.github.hgkmail.hello.cmd.helper.UniqueIndex;

import java.math.BigDecimal;
import java.util.Date;

/**
 * table 公会盛典收益任务表
 *
 * @author kimhuang
 * @date 2022/11/17
 *
 * @table t_family_income_celebration_result
 * @comment 公会盛典收益任务表
 * @charset utf8
 */
public abstract class FamilyIncomeCelebrationResult {

    ///fields///

    /**
     * @primary
     * @dbType bigint(20)
     * @dbAttr NOT NULL
     * @comment id
     * @transfer Long id 主键
     */
    Long id;

    /**
     * @dbType bigint(20)
     * @comment 公会id
     * @dbDefault '0'
     * @cond chooseClanId
     * @condType Long
     */
    Long clanId;

    /**
     * @dbType int(11)
     * @dbDefault '0'
     * @default 0
     * @cond
     * @comment 类型，0:2022公会盛典
     * @enum 0:2022公会盛典:ceremony2022
     */
    Integer type;

    /**
     * @dbType tinyint(4)
     * @dbDefault '0'
     * @default 0
     * @cond
     * @comment 赛道，1:黄金, 2:钻石
     * @enum 1:黄金:gold, 2:钻石:diamond
     */
    Integer track;

    /**
     * @dbType varchar(50)
     * @dbDefault ''
     * @default ""
     * @view _ stepName 当前阶段名称
     * @transfer _ _ 当前阶段名称
     */
    String stepName;

    /**
     * @dbType decimal(32,2)
     * @comment 基准收入值
     * @dbDefault '0.00'
     */
    BigDecimal baseIncome;

    /**
     * @dbType decimal(32,2)
     * @comment 第一档收入值
     * @dbDefault '0.00'
     */
    BigDecimal firstTarget;

    /**
     * @dbType decimal(32,2)
     * @comment 第一档奖励比例
     * @dbDefault '0.00'
     */
    BigDecimal firstAwardRatio;

    /**
     * @dbType decimal(32,2)
     * @comment 第一档奖励金额
     * @dbDefault '0.00'
     */
    BigDecimal firstAwardIncome;

    /**
     * @dbType decimal(32,2)
     * @comment 第二档收入值
     * @dbDefault '0.00'
     */
    BigDecimal secondTarget;

    /**
     * @dbType decimal(32,2)
     * @comment 第二档奖励比例
     * @dbDefault '0.00'
     */
    BigDecimal secondAwardRatio;

    /**
     * @dbType decimal(32,2)
     * @comment 第二档奖励金额
     * @dbDefault '0.00'
     */
    BigDecimal secondAwardIncome;

    /**
     * @dbType decimal(32,2)
     * @comment 第三档收入值
     * @dbDefault '0.00'
     */
    BigDecimal thirdTarget;

    /**
     * @dbType decimal(32,2)
     * @comment 第三档奖励比例
     * @dbDefault '0.00'
     */
    BigDecimal thirdAwardRatio;

    /**
     * @dbType decimal(32,2)
     * @comment 第三档奖励金额
     * @dbDefault '0.00'
     */
    BigDecimal thirdAwardIncome;

    /**
     * @dbType decimal(32,2)
     * @comment 当前收入
     * @dbDefault '0.00'
     */
    BigDecimal income;

    /**
     * @dbType decimal(32,2)
     * @comment 当前奖励金额
     * @dbDefault '0.00'
     */
    BigDecimal awardIncome;

    /**
     * @dbType bigint(20)
     * @comment 添加时间戳
     * @dbDefault '0'
     */
    Long createTime;

    /**
     * @dbType bigint(20)
     * @comment 更新时间戳
     * @dbDefault '0'
     */
    Long updateTime;

    ///indexes///

    abstract UniqueIndex uniq_clanId_type(IndexKey clanId, IndexKey type);

}
