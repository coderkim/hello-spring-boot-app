package com.github.hgkmail.hello.entity.generate;

import com.github.hgkmail.hello.cmd.helper.IndexKey;
import com.github.hgkmail.hello.cmd.helper.NormalIndex;
import com.github.hgkmail.hello.cmd.helper.UniqueIndex;

import java.math.BigDecimal;
import java.util.Date;

/**
 * table 公会收益周任务每日进度表
 *
 * @author kimhuang
 * @date 2021/12/12
 *
 * @table t_family_biz_clan_week_income_progress
 * @comment 公会收益周任务每日进度表
 * @charset utf8
 */
public abstract class ClanWeekIncomeProgress {

    ///fields///

    /**
     * @primary
     * @dbType bigint(20) unsigned
     * @dbAttr NOT NULL
     * @comment 全局id
     * @transfer
     */
    Long id;

    /**
     * @dbType bigint(20) unsigned
     * @dbAttr NOT NULL
     * @comment 公会id
     * @transfer
     */
    Long clanId;

    /**
     * @dbType int(10) unsigned
     * @dbAttr NOT NULL
     * @comment 更新日期
     * @transfer
     */
    Integer date;

    /**
     * @dbType int(10) unsigned
     * @dbAttr NOT NULL
     * @comment 年
     * @transfer
     */
    Integer year;

    /**
     * @dbType int(10) unsigned
     * @dbAttr NOT NULL
     * @comment 周
     * @transfer
     */
    Integer week;

    /**
     * @dbType decimal(32,2)
     * @dbAttr NOT NULL
     * @comment 当周累计充值账户消费流水
     * @transfer
     */
    BigDecimal consumeMoneyCoin;

    /**
     * @dbType decimal(32,2)
     * @dbAttr NOT NULL
     * @comment 当周累计周均星豆
     * @transfer
     */
    BigDecimal bean;

    /**
     * @dbType timestamp
     * @comment 创建时间
     * @dbAttr NOT NULL DEFAULT CURRENT_TIMESTAMP
     * @transfer
     */
    Date createTime;

    ///indexes///

    abstract UniqueIndex uniq_clanId_date(IndexKey clanId, IndexKey date);

    abstract NormalIndex idx_year_week(IndexKey year, IndexKey week);

}
