package com.github.hgkmail.hello.entity.employees;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;

public class SalaryStatsRecord {
    private Integer emp_no;
    private Date start_date;
    private Date end_date;
    private BigDecimal total_salary;
    private BigDecimal average_salary;
    private Integer year_num;
    private Timestamp create_time;

    public SalaryStatsRecord(Integer emp_no) {
        this.emp_no = emp_no;

        //not null约束的field需要填充默认值
        this.start_date = new Date(0);
        this.end_date = new Date(0);
        this.total_salary = BigDecimal.ZERO;
        this.average_salary = BigDecimal.ZERO;
        this.year_num = 0;
    }

    public Integer getEmp_no() {
        return emp_no;
    }

    public void setEmp_no(Integer emp_no) {
        this.emp_no = emp_no;
    }

    public Date getStart_date() {
        return start_date;
    }

    public void setStart_date(Date start_date) {
        this.start_date = start_date;
    }

    public Date getEnd_date() {
        return end_date;
    }

    public void setEnd_date(Date end_date) {
        this.end_date = end_date;
    }

    public BigDecimal getTotal_salary() {
        return total_salary;
    }

    public void setTotal_salary(BigDecimal total_salary) {
        this.total_salary = total_salary;
    }

    public BigDecimal getAverage_salary() {
        return average_salary;
    }

    public void setAverage_salary(BigDecimal average_salary) {
        this.average_salary = average_salary;
    }

    public Integer getYear_num() {
        return year_num;
    }

    public void setYear_num(Integer year_num) {
        this.year_num = year_num;
    }

    public Timestamp getCreate_time() {
        return create_time;
    }

    public void setCreate_time(Timestamp create_time) {
        this.create_time = create_time;
    }

    @Override
    public String toString() {
        return "SalaryStatsRecord{" +
                "emp_no=" + emp_no +
                ", start_date=" + start_date +
                ", end_date=" + end_date +
                ", total_salary=" + total_salary +
                ", average_salary=" + average_salary +
                ", year_num=" + year_num +
                ", create_time=" + create_time +
                '}';
    }
}
