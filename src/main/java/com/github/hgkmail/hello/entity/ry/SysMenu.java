package com.github.hgkmail.hello.entity.ry;

import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;

/**
 * 因为MyBatis不是ORM，所以entity的类名和字段名可以不受拘束，到时在resultMap配置好映射即可
 */
public class SysMenu {

    private Long id;
    private String name;
    private Long parentId;
    private Integer order;
    @Pattern(regexp = "/.+/.+", message = "url不合法")
    private String url;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "SysMenu{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", parentId=" + parentId +
                ", order=" + order +
                ", url='" + url + '\'' +
                '}';
    }
}
