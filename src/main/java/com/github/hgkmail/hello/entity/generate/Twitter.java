package com.github.hgkmail.hello.entity.generate;

import com.github.hgkmail.hello.cmd.helper.IndexKey;
import com.github.hgkmail.hello.cmd.helper.NormalIndex;
import com.github.hgkmail.hello.cmd.helper.UniqueIndex;

import java.util.Date;

/**
 * table 微博
 *
 * @author kimhuang
 * @date 2021/2/12
 *
 * @table t_twitter
 * @comment 微博表
 * @charset utf8
 */
public abstract class Twitter {

    ///fields///

    /**
     * @primary
     * @dbType bigint(20) unsigned
     * @dbAttr NOT NULL, AUTO_INCREMENT
     * @comment 自增id
     * @useGenKey
     * @transfer Long tid 微博id
     */
    Long id;

    /**
     * @dbType varchar(20)
     * @dbDefault ''
     * @default ""
     * @view _ head 标题
     * @transfer _ _ 标题
     */
    String title;

    /**
     * @dbType varchar(140)
     * @dbDefault ''
     * @default ""
     * @view _ _ 内容
     * @transfer _ _ 内容
     */
    String content;

    /**
     * @dbType varchar(20)
     * @dbAttr NOT NULL
     * @dbDefault ''
     * @cond
     * @view _ writer 作者
     * @transfer _ writer 作者
     */
    String author;

    /**
     * @dbType int(10)
     * @comment 创建年月
     * @dbDefault '0'
     * @cond chooseYearMonth
     * @condType Long
     */
    Integer yearMonth;

    /**
     * @dbType tinyint(1)
     * @dbDefault '0'
     * @default 0
     * @cond
     * @comment 微博状态，0:草稿, 1:已发布, 2:删除, -1:被封
     * @enum 0:草稿:draft, 1:已发布:published, 2:删除:deleted, -1:被封:banned
     */
    Integer status;

    /**
     * @dbType varchar(40)
     * @comment UUID, 通用唯一识别码, 包含4个横杆有36位
     * @dbDefault ''
     */
    String uuid;

    /**
     * @dbType timestamp
     * @comment 创建时间
     * @dbAttr NOT NULL
     * @condRange createTimeStart, createTimeEnd
     * @condType String
     * @view String createTimeStr 添加时间 DateUtil.format($,DateUtil.YYYY_MM_DD_TIME)
     * @transfer Long createTimestamp 创建时间戳 DateUtil.getUnixTimestamp($)
     */
    Date createTime;

    /**
     * @dbType timestamp
     * @comment 更新时间
     * @dbDefault CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
     * @dbTrigger
     */
    Date updateTime;

    ///indexes///

    abstract NormalIndex author(IndexKey author);

    abstract NormalIndex year_month_status(IndexKey yearMonth, IndexKey status);

    abstract NormalIndex create_time(IndexKey createTime);

    abstract UniqueIndex uuid(IndexKey uuid);

}
