package com.github.hgkmail.hello.entity.generate;

import com.github.hgkmail.hello.cmd.helper.IndexKey;
import com.github.hgkmail.hello.cmd.helper.NormalIndex;
import com.github.hgkmail.hello.cmd.helper.UniqueIndex;

import java.math.BigDecimal;
import java.util.Date;

/**
 * table 非核1库申请列表
 *
 * @author kimhuang
 * @date 2022/02/27
 *
 * @table t_clan_apply
 * @comment 非核1库申请列表
 * @charset utf8
 */
public abstract class NoncoreClanApply {

    ///fields///

    /**
     * @primary
     * @dbType bigint(20)
     * @dbAttr NOT NULL
     * @comment 用户ID
     * @transfer
     */
    Long userId;

    /**
     * @dbType int(10) unsigned
     * @dbAttr NOT NULL
     * @comment 公会id
     * @transfer
     */
    Long clanId;

    /**
     * @dbType int(11)
     * @dbAttr NOT NULL
     * @comment 申请时间
     * @transfer
     */
    Integer applyTime;

    /**
     * @dbType tinyint(4)
     * @dbAttr NOT NULL
     * @comment 状态(0:未处理,1:通过,-1:拒绝)
     * @enum 0:未处理:pending, 1:通过:pass, -1:拒绝:reject
     * @transfer
     */
    Integer status;

    ///indexes///

    abstract NormalIndex clanId(IndexKey clanId);

    abstract NormalIndex idx_applyTime_status(IndexKey applyTime, IndexKey status);

}
