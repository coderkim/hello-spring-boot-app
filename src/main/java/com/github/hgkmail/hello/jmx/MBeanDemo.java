package com.github.hgkmail.hello.jmx;

import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.jmx.export.annotation.ManagedResource;
import org.springframework.stereotype.Component;

@Component
@ManagedResource(objectName = "com.github.hgkmail.hello.jmx:name=MBeanDemo")
public class MBeanDemo {
    private String name="MBean1";

    @ManagedAttribute
    public String getName() {
        return name;
    }

    @ManagedAttribute
    public void setName(String name) {
        this.name = name;
    }

    @ManagedOperation
    public void shutdown() {
        System.exit(0);
    }
}
