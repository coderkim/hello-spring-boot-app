package com.github.hgkmail.hello.annotation;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Documented
@Inherited
public @interface PrintMethod {
    boolean showArgs() default false;
}
