package com.github.hgkmail.hello.annotation;

import java.lang.annotation.*;

/**
 * 散步
 * 请求开始时和请求结束时都会打印log
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD})
@Documented
@Inherited
public @interface Walk {
    String word() default "walk";
}
