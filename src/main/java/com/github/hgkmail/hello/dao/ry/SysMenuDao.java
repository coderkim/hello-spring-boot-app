package com.github.hgkmail.hello.dao.ry;

import com.github.hgkmail.hello.entity.ry.SysMenu;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.type.JdbcType;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface SysMenuDao {

    @Cacheable(cacheNames = {"sys.menu.page"}, keyGenerator = "keyGenerator")
    @Select("SELECT * from sys_menu limit #{offset},#{limit}")
    @Results(id = "sysMenu", value = {
            @Result(column = "menu_id", property = "id", javaType = Long.class, jdbcType = JdbcType.BIGINT),
            @Result(column = "menu_name", property = "name", javaType = String.class, jdbcType = JdbcType.VARCHAR),
            @Result(column = "parent_id", property = "parentId", javaType = Long.class, jdbcType = JdbcType.BIGINT),
            @Result(column = "order_num", property = "order", javaType = Integer.class, jdbcType = JdbcType.INTEGER),
            @Result(column = "url", property = "url", javaType = String.class, jdbcType = JdbcType.VARCHAR)
    })
    List<SysMenu> findByPage(@Param("offset") int offset, @Param("limit") int limit);

    @Select("SELECT * from sys_menu")
    @ResultMap("sysMenu")
    List<SysMenu> findAll();

    @Select({"<script>",
            "SELECT * ",
            "from sys_menu",
            "<where>",
            "<if test='id!=null and id!=0'>",
            "menu_id = #{id}",
            "</if>",
            "</where>",
            "</script>"})
    @ResultMap("sysMenu")
    SysMenu getById(@Param("id")Integer id);

    @Update("update sys_menu set menu_name=#{menu.name},parent_id=#{menu.parentId},order_num=#{menu.order},url=#{menu.url} where menu_id=#{menu.id}")
    void updateById(@Param("menu")SysMenu menu);

}
