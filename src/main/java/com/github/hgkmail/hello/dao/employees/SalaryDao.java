package com.github.hgkmail.hello.dao.employees;

import com.github.hgkmail.hello.entity.employees.Salary;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface SalaryDao {
    @Select({"<script>",
            "SELECT * ",
            "from salaries",
            "<where>",
            "<if test='empNo!=null and empNo!=0'>",
            "emp_no = #{empNo}",
            "</if>",
            "</where>",
            "</script>"})
    List<Salary> getSalaryByEmpNo(@Param("empNo")Integer empNo);
}
