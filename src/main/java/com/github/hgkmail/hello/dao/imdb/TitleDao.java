package com.github.hgkmail.hello.dao.imdb;

import com.github.hgkmail.hello.entity.imdb.AkaTitle;
import com.github.hgkmail.hello.entity.imdb.BasicTitle;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface TitleDao {

    @Select({"<script>",
            "SELECT * ",
            "from t_title_basics",
            "<where>",
            "<if test='pk!=null'>",
            "tconst = #{pk}",
            "</if>",
            "</where>",
            "</script>"})
    BasicTitle getBasicByPk(@Param("pk")String pk);

    @Select({"<script>",
            "SELECT * ",
            "from t_title_akas",
            "<where>",
            "<if test='titleId!=null'>",
            "titleId = #{titleId}",
            "</if>",
            "</where>",
            "</script>"})
    List<AkaTitle> getAkaByTitleId(@Param("titleId")String titleId);

}
