package com.github.hgkmail.hello.dao.employees;

import com.github.hgkmail.hello.entity.employees.Employee;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface EmployeesDao {

    @Select({"<script>",
            "SELECT * ",
            "from employees",
            "<where>",
            "<if test='empNo!=null and empNo!=0'>",
            "emp_no = #{empNo}",
            "</if>",
            "</where>",
            "</script>"})
    Employee getByEmpNo(@Param("empNo")Integer empNo);

    @Insert({"<script>",
            "insert into employees values ",
            "(#{emp_no}, #{birth_date}, #{first_name}, #{last_name}, #{gender}, #{hire_date})",
            "</script>"})
    int save(Employee e);

    @Select({"<script>",
            "SELECT * ",
            "from employees",
            "where emp_no>#{start} order by emp_no asc limit #{size}",
            "</script>"})
    List<Employee> getByPage(@Param("start")Integer start, @Param("size")Integer size);

    @Select({"<script>",
            "SELECT count(1)",
            "from employees",
            "</script>"})
    int countAll();

}
