package com.github.hgkmail.hello.dao.employees;

import com.github.hgkmail.hello.entity.employees.SalaryStatsRecord;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface SalaryStatsDao {

    @Insert({"<script>",
            "insert into t_salary_stats(emp_no,start_date,end_date,total_salary,average_salary,year_num) values",
            "(#{r.emp_no},#{r.start_date},#{r.end_date},#{r.total_salary},#{r.average_salary},#{r.year_num})",
            "</script>"})
    int saveRecord(@Param("r") SalaryStatsRecord record);

    @Insert({"<script>",
            "insert into t_salary_stats(emp_no,start_date,end_date,total_salary,average_salary,year_num) values",
            "<foreach collection ='rs' item='r' separator =','>",
            "(#{r.emp_no},#{r.start_date},#{r.end_date},#{r.total_salary},#{r.average_salary},#{r.year_num})",
            "</foreach >",
            "</script>"})
    int batchSaveRecord(@Param("rs") List<SalaryStatsRecord> records);

    @Delete({"<script>",
            "delete from t_salary_stats where emp_no=#{empNo}",
            "</script>"})
    int deleteByEmpNo(@Param("empNo")Integer empNo);

    @Delete({"<script>",
            "delete from t_salary_stats where emp_no &lt;= #{empNo}",
            "</script>"})
    int deleteBefore(@Param("empNo")Integer empNo);

}
