package com.github.hgkmail.hello.constant;

public class CommonResponse {

    public static final int CODE_SUCCESS = 0;

    public static final String MSG_SUCCESS = "操作成功";

    public static final int CODE_FAIL = -1;

    public static final String MSG_FAIL = "网络异常，请稍后再试";

}
