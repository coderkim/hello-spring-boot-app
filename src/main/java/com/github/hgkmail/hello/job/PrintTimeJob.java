package com.github.hgkmail.hello.job;

import com.dianping.cat.Cat;
import com.dianping.cat.message.Event;
import com.dianping.cat.message.Transaction;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.QuartzJobBean;

import java.text.SimpleDateFormat;
import java.util.Date;

import static com.dianping.cat.message.Message.SUCCESS;

public class PrintTimeJob extends QuartzJobBean {

    private static final Logger logger = LoggerFactory.getLogger(PrintTimeJob.class);

    @Override
    protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
        Transaction t = Cat.newTransaction("QuartzJob", "PrintTimeJob");

        try {
            String greet = (String)context.getJobDetail().getJobDataMap().get("greet");
            Date date = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String dateStr = sdf.format(date);
            logger.info("{} {}", dateStr, greet);

            Cat.logEvent("QuartzJob.execute", "PrintTimeJob", SUCCESS, String.format("greet=%s&date=%s", greet, dateStr));
            Cat.logMetricForCount("PrintTimeJob.times");
            t.setStatus(SUCCESS);
        } catch (Exception e) {
            t.setStatus(e);
            Cat.logError(e);
        } finally {
            t.complete();
        }
    }

}
