package com.github.hgkmail.hello.web;

import com.github.hgkmail.hello.constant.CommonResponse;
import com.github.hgkmail.hello.vo.BaseVo;
import com.google.common.base.CharMatcher;
import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

import static com.github.hgkmail.hello.constant.CommonResponse.*;

@RestController
@RequestMapping("/guava")
public class GuavaController {

    private static final Joiner joiner = Joiner.on("_").useForNull("null");
    private static final Splitter splitter = Splitter.on(",").trimResults().omitEmptyStrings();

    @GetMapping("/list")
    public BaseVo hello() {
//        ArrayList list = Lists.newArrayList("aaa",null,123);
        List<String> strs = Lists.newArrayList(splitter.split("aa,,bb,   ,    c"));
        String ip = "127   0    0    1";
        String res = CharMatcher.whitespace().collapseFrom(ip,'.');
        return new BaseVo(CODE_SUCCESS, MSG_SUCCESS, res);
    }

}
