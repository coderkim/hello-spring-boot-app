package com.github.hgkmail.hello.exception;

public class CommonException extends Exception {

    public CommonException(String msg) {
        super(msg);
    }

}
