package com.github.hgkmail.hello.dubbo.impl;

import com.github.hgkmail.hello.dubbo.HelloDubboService;
import org.apache.dubbo.config.annotation.DubboService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@DubboService(version = "1.0.0")
public class HelloDubboServiceImpl implements HelloDubboService {
    private static final Logger logger = LoggerFactory.getLogger(HelloDubboServiceImpl.class);

    @Override
    public Integer plus(Integer a, Integer b) {
        logger.info("plus {} and {}", a, b);
        return a+b;
    }

    @Override
    public String sayHello(String name) {
        logger.info("say hello to {}", name);
        return String.format("Hello, %s", name);
    }

}
