package com.github.hgkmail.hello.dubbo;

public interface HelloDubboService {

    Integer plus(Integer a, Integer b);

    String sayHello(String name);

}
