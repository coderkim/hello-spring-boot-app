package com.github.hgkmail.hello.vo;

public class LayuiTableVo extends BaseVo {

    /**
     * 总数
     */
    private Long count;

    public LayuiTableVo() {}

    public LayuiTableVo(Integer code, String msg, Object data, Long count) {
        super(code, msg, data);
        this.count = count;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "LayuiTableVo{" +
                "count=" + count +
                ", code=" + code +
                ", msg='" + msg + '\'' +
                ", data=" + data +
                '}';
    }

}
