package com.github.hgkmail.hello.interview.jvm.gc;

import java.lang.reflect.Proxy;
import java.util.Scanner;

public class AllocationTest {

    private static final int _1MB = 1024 * 1024;

    /**
     * 验证：对象优先在Eden分配
     * VM 参 数：-verbose:gc -XX:+PrintGCDetails -Xms20M -Xmx20M -Xmn10M -XX:SurvivorRatio=8 -XX:+UseParallelGC
     */
    public static void testAllocation() {
        Scanner scanner = new Scanner(System.in);

        byte[] allocation1, allocation2, allocation3, allocation4;

        allocation1 = new byte[2 * _1MB];
        scanner.next();

        allocation2 = new byte[2 * _1MB];
        scanner.next();

        allocation3 = new byte[2 * _1MB];
        scanner.next();

        allocation4 = new byte[4 * _1MB]; //出现一次Minor GC
        scanner.next();
    }

    /**
     * 验证：大对象直接进入老年代
     * VM 参 数：-verbose:gc -XX:+PrintGCDetails -Xms20M -Xmx20M -Xmn10M -XX:SurvivorRatio=8 -XX:+UseParallelGC -XX:PretenureSizeThreshold=3145728
     */
    public static void testPretenureSizeThreshold() {
        byte[] allocation;
        allocation = new byte[4 * _1MB]; //直接分配在老年代中
    }

    public static void main(String[] args) {
        testAllocation();
    }

}
