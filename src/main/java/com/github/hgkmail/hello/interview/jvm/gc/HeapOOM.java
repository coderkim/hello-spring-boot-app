package com.github.hgkmail.hello.interview.jvm.gc;

import java.util.ArrayList;
import java.util.List;

//Java堆内存溢出异常测试
//vm args: -Xms20m -Xmx20m -XX:+HeapDumpOnOutOfMemoryError
//visualvm很好用，visualvm + dump文件，可以查看 内存分配 和 线程执行情况，一般够用了。
public class HeapOOM {
    static class OOMObject {}

    public static void main(String[] args) {
        List<OOMObject> list=new ArrayList<>();
        while (true) {
            list.add(new OOMObject());
        }
    }
}
