package com.github.hgkmail.hello.interview.jvm.classloader;

import org.apache.commons.lang3.reflect.FieldUtils;

import java.lang.reflect.Field;
import java.net.URL;
import java.net.URLClassLoader;

//可以实现插件机制
//1.插件类都放到同一个目录，比如plugins/下面
//2.插件类都实现同一个接口，比如IPlugin接口
//3.把插件类的完整类名添加到一个名单（或者扫描目录）
//系统启动时，根据名单去约定目录下加载类，根据接口逐个进行调用，tomcat加载JavaWeb应用等都是这样做的
public class URLClassLoaderTest {

    // javac多文件编译(必须这样编译，直接javac xxx.java编译出来包名不正确)
    // cd src/
    // javac -d ../out pkg1/pkg2/*.java
    public static void main(String[] args) {
        try {
            URLClassLoader urlClassLoader=new URLClassLoader(new URL[]{
                    new URL("file:///Users/hgkmail-mb/github/hello-spring-boot-app/src/main/resources/javactest/out/") //类搜索路径
            });
            Class<?> cls = urlClassLoader.loadClass("pkg1.pkg2.SimpleKey"); //根据类名加载类

            Field field = FieldUtils.getField(cls, "key", true); //强制获取私有字段（这只是一个field对象）
            Object instance = cls.newInstance(); //实例化
            Object value = field.get(instance); //获取字段的值

            System.out.println(instance);
            System.out.println(value);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
