package com.github.hgkmail.hello.interview.jvm.gc;

//StackOverflowError（SOF）测试
//vm args: -Xss1m
//溢出时 stackLength 的大小与Xss的配置有关，正相关
public class JavaVMStackSOF {
    private int stackLength=1;
    public void stackLeak() {
        stackLength++;
        stackLeak();
    }

    public static void main(String[] args) {
        JavaVMStackSOF sof=new JavaVMStackSOF();
        try {
            sof.stackLeak();
        } catch (Throwable e) {
            System.out.println("stack length:"+ sof.stackLength);
            throw e;
        }
    }
}
