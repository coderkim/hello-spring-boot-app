package com.github.hgkmail.hello.cmd.helper;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.List;

/**
 * 数据对象信息
 * Data Object
 *
 * @author kimhuang
 * @date 2021/2/14
 */
@Data
@Accessors(chain = true)
public class EntityInfo {

    private String name;
    private String varName;
    private String listName;
    private String table;
    private List<String> allFields;
    private String allFieldsPart;
    private List<String> manualFields;
    private String manualFieldsPart;
    private String manualFieldsBracketPart;
    private String manualFieldsParamPart;

    private String primaryType;
    private String primaryField;
    private String primaryFieldU;
    private String primaryList;
    private String primaryColumn;
    private boolean useGenKey;

    public EntityInfo() {
        //字段名称
        this.allFields = new ArrayList<>();
        this.manualFields = new ArrayList<>();
    }

}
