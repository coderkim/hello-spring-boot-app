package com.github.hgkmail.hello.cmd.helper;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * DAO Provider 类信息
 *
 * @author kimhuang
 * @date 2021/2/15
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
public class DaoProviderClazzInfo extends ClazzInfo {

    private String table;
    private List<String> allFields;
    private String allFieldsPart;

}
