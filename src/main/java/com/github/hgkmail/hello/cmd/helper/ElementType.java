package com.github.hgkmail.hello.cmd.helper;

/**
 * 元素类型
 *
 * @author kimhuang
 * @date 2021/2/13
 */
public enum ElementType {
    FIELD, METHOD
}
