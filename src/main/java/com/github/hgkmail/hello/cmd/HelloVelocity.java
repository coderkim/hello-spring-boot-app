package com.github.hgkmail.hello.cmd;

import com.github.hgkmail.hello.util.VelocityUtil;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;

import java.io.StringWriter;
import java.util.*;

public class HelloVelocity {

    public static void main(String[] args) {
        VelocityEngine ve = new VelocityEngine();
        ve.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
        ve.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());
        ve.init();

        // 载入（获取）模板对象
        Template t = ve.getTemplate("code-maker/HelloWorld.vm");
        VelocityContext ctx = new VelocityContext();
        // 域对象加入参数值
        ctx.put("name", "李智龙");
        ctx.put("date", (new Date()).toString());
        // list集合
        List<String> temp = new ArrayList<>();
        temp.add("1");
        temp.add("2");
        ctx.put("list", temp);
        // map
        Map<String, Object> map = new HashMap<>();
        map.put("aaa", "bbb");
        ctx.put("map", map);

        ctx.put("util", VelocityUtil.class);

        StringWriter sw = new StringWriter();
        t.merge(ctx, sw);

        System.out.println(sw.toString());
    }

}
