package com.github.hgkmail.hello.cmd.helper;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 索引信息
 *
 * @author kimhuang
 * @date 2021/2/13
 */
@Data
@Accessors(chain = true)
public class IndexInfo {

    private String name;
    private List<String> columns;
    private String columnsPart;
    private String typePart;

}
