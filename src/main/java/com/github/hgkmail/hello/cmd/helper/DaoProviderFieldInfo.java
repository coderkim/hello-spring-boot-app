package com.github.hgkmail.hello.cmd.helper;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * class
 *
 * @author kimhuang
 * @date 2021/2/15
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
public class DaoProviderFieldInfo extends FieldInfo {

    /**
     * SQL中比较操作符，比如"=", ">", "<"
     * 不支持生成">="，可以手动改
     */
    private String compareOperator;
    private String getter;
    private String setter;
    private String column;

}
