package com.github.hgkmail.hello.cmd.helper;

import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.thoughtworks.qdox.model.DocletTag;
import com.thoughtworks.qdox.model.JavaAnnotatedElement;
import com.thoughtworks.qdox.model.JavaClass;
import org.apache.commons.lang3.StringUtils;

import java.util.Collection;
import java.util.List;

/**
 * class
 *
 * @author kimhuang
 * @date 2021/2/13
 */
public class DocTagUtil {

    public static final String WHITE_SPACE = " ";
    public static final String COMMA = ", ";

    public static String firstTagValue(JavaAnnotatedElement el, String tagName, String defaultValue) {
        if (el!=null) {
            DocletTag tag = el.getTagByName(tagName);
            if (tag!=null && StringUtils.isNotBlank(tag.getValue())) {
                return tag.getValue();
            }
        }
        return defaultValue;
    }

    public static List<String> firstTagParams(JavaAnnotatedElement el, String tagName, List<String> defaultParams) {
        if (el!=null) {
            DocletTag tag = el.getTagByName(tagName);
            if (tag!=null) {
                String value = tag.getValue();
                return Splitter.on(",").omitEmptyStrings().trimResults().splitToList(value);
            }
        }
        return defaultParams;
    }

    public static String getIndexPrefix(JavaClass returnClass) {
        if (returnClass==null) {
            return "idx_";
        }
        String s = returnClass.getSimpleName();
        if ("UniqueIndex".equals(s)) {
            return "uniq_";
        } else {
            return "idx_";
        }
    }

    public static String getIndexTypePart(JavaClass returnClass) {
        if (returnClass==null) {
            return "KEY";
        }
        String s = returnClass.getSimpleName();
        if ("UniqueIndex".equals(s)) {
            return "UNIQUE KEY";
        } else {
            return "KEY";
        }
    }

    /**
     * 列表取元素，支持默认值
     */
    public static <T> T getAt(List<T> co, int pos, T defaultVal) {
        if (co.size()>=pos+1) {
            return co.get(pos);
        }
        return defaultVal;
    }

    /**
     * 列表取元素，支持默认值，会忽略占位符下划线"_"
     */
    public static <T> T getAtIfNotHolder(List<T> co, int pos, T defaultVal) {
        String placeHolder = "_";

        if (co.size()>=pos+1) {
            T item = co.get(pos);
            if (!placeHolder.equals(item)) {
                return item;
            }
        }

        return defaultVal;
    }

    /**
     * 首字母转小写
     */
    public static String toLowerCaseFirstOne(String s){
        if(Character.isLowerCase(s.charAt(0))) {
            return s;
        }
        return (new StringBuilder()).append(Character.toLowerCase(s.charAt(0))).append(s.substring(1)).toString();
    }


    /**
     * 首字母转大写
     */
    public static String toUpperCaseFirstOne(String s){
        if(Character.isUpperCase(s.charAt(0))) {
            return s;
        }
        return (new StringBuilder()).append(Character.toUpperCase(s.charAt(0))).append(s.substring(1)).toString();
    }

    private DocTagUtil() {}
}
