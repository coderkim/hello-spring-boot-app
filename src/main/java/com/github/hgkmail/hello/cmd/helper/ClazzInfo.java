package com.github.hgkmail.hello.cmd.helper;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 类信息
 *
 * @author kimhuang
 * @date 2021/2/14
 */
@Data
@Accessors(chain = true)
public class ClazzInfo {

    protected String name;
    protected String author;
    protected String date;
    protected String comment;

}
