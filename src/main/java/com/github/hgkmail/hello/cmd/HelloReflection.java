package com.github.hgkmail.hello.cmd;

import com.github.hgkmail.hello.util.VelocityUtil;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;

import java.io.StringWriter;
import java.lang.reflect.Method;
import java.util.*;

/**
 * 通过反射调用java任意类的任意方法
 */
public class HelloReflection {

    public static void main(String[] args) throws Exception {
        Class<?> clazz = Class.forName("com.github.hgkmail.hello.entity.test.Person");
        Method[] methods = clazz.getDeclaredMethods();
        for (Method method:methods) {
            if (method.getName().equals("sayHello")) {
                List<Object> params = new ArrayList<>();
                params.add("tom");
                params.add(18);
                Object res = method.invoke(clazz.newInstance(), params.toArray());
                System.out.println(res);
            }
        }
    }

}
