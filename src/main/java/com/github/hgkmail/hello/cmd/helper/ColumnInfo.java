package com.github.hgkmail.hello.cmd.helper;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 列信息
 *
 * @author kimhuang
 * @date 2021/2/13
 */
@Data
@Accessors(chain = true)
public class ColumnInfo {

    private String name;
    private String dbType;
    private String comment;
    private List<String> dbAttrs;
    private String dbAttrsPart;
    private String dbDefault;
    private String dbDefaultPart;

}
