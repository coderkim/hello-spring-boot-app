package com.github.hgkmail.hello.cmd.helper;

import com.thoughtworks.qdox.model.JavaClass;
import com.thoughtworks.qdox.model.JavaField;
import com.thoughtworks.qdox.model.JavaMethod;

/**
 * 解析Java class事件监听器
 * 解析出错抛异常
 *
 * @author kimhuang
 * @date 2021/2/13
 */
public interface ParseClassListener {
    void onStart(JavaClass jClass);
    void onField(JavaClass jClass, JavaField jField);
    void onMethod(JavaClass jClass, JavaMethod jMethod);
    void onEnd(JavaClass jClass);
}
