package com.github.hgkmail.hello.cmd;

import com.github.hgkmail.hello.cmd.helper.*;
import com.google.common.base.Splitter;
import com.thoughtworks.qdox.model.JavaClass;
import com.thoughtworks.qdox.model.JavaField;
import com.thoughtworks.qdox.model.JavaMethod;
import org.apache.commons.lang3.StringUtils;
import org.apache.velocity.VelocityContext;

import java.util.ArrayList;
import java.util.List;

import static java.lang.System.out;

/**
 * 生成 DTO.java
 * 格式：@transfer type name title formatter 注意每个字段都不能有空格，包含formatter
 *
 * @author kimhuang
 * @date 2021/2/12
 */
public class GenDataTransferObject extends BaseParseClass {

    public static final String TAG_TRANSFER = "transfer";

    public static void main(String[] args) {
        new GenDataTransferObject().run();
    }

    @Override
    public void run() {
        parseClass(new RenderDataTransferObject());
    }

    private class RenderDataTransferObject implements ParseClassListener {
        private ClazzInfo clazz = new ClazzInfo();
        private List<ViewObjectFieldInfo> generalFields = new ArrayList<>();

        RenderDataTransferObject() {
        }

        @Override
        public void onStart(JavaClass jClass) {
            clazz.setName(jClass.getSimpleName());
            clazz.setAuthor(DocTagUtil.firstTagValue(jClass, "author", ""));
            clazz.setDate(DocTagUtil.firstTagValue(jClass, "date", ""));
            clazz.setComment(DocTagUtil.firstTagValue(jClass, "comment", ""));
        }

        @Override
        public void onField(JavaClass jClass, JavaField jField) {
            if (jField.getTagByName(TAG_TRANSFER)==null) {
                return;
            }
            String viewTagVal = DocTagUtil.firstTagValue(jField, TAG_TRANSFER, "");
            List<String> viewTagVals = Splitter.on(DocTagUtil.WHITE_SPACE).omitEmptyStrings().splitToList(viewTagVal);

            ViewObjectFieldInfo fieldInfo = new ViewObjectFieldInfo();
            fieldInfo.setType(DocTagUtil.getAtIfNotHolder(viewTagVals, 0, jField.getType().getSimpleName()));
            fieldInfo.setName(DocTagUtil.getAtIfNotHolder(viewTagVals, 1, jField.getName()));
            fieldInfo.setTitle(DocTagUtil.getAtIfNotHolder(viewTagVals, 2, jField.getComment()));
            fieldInfo.setEntityGetter(String.format("get%s()", DocTagUtil.toUpperCaseFirstOne(jField.getName())));
            fieldInfo.setEntityName(jField.getName());

            String formatter = DocTagUtil.getAtIfNotHolder(viewTagVals, 3, "");
            if (StringUtils.isNotBlank(formatter)) {
                fieldInfo.setFormatEntityGetter(formatter.replaceAll("\\$", "entity."+fieldInfo.getEntityGetter()));
                fieldInfo.setHasFormatter(true);
            } else {
                fieldInfo.setFormatEntityGetter("");
                fieldInfo.setHasFormatter(false);
            }

            generalFields.add(fieldInfo);
        }

        @Override
        public void onMethod(JavaClass jClass, JavaMethod jMethod) {
            //nothing
        }

        @Override
        public void onEnd(JavaClass jClass) {
            renderVm("code-maker/generate/data_transfer_object_java.vm", new RenderListener() {
                @Override
                public void beforeRender(VelocityContext vContext) {
                    vContext.put("clazz", clazz);
                    vContext.put("generalFields", generalFields);
                }

                @Override
                public void afterRender(VelocityContext vContext, String result) {
                    out.println(result);
                }
            });
        }

    }

}
