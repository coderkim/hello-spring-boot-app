package com.github.hgkmail.hello.cmd.helper;

import java.lang.annotation.Documented;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * 元信息
 *
 * @author kimhuang
 * @date 2021/2/12
 */
@Target({ TYPE, FIELD, METHOD })
@Retention(RUNTIME)
@Repeatable(Meta.List.class)
@Documented
public @interface Meta {

    /**
     * key
     */
    String k() default "";
    /**
     * value 空格分隔表示列表
     */
    String v()  default "";

    @Target({ TYPE, FIELD, METHOD })
    @Retention(RUNTIME)
    @Documented
    @interface List {
        Meta[] value();
    }

}
