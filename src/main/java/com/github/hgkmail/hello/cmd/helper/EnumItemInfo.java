package com.github.hgkmail.hello.cmd.helper;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * class
 *
 * @author kimhuang
 * @date 2021/2/15
 */
@Data
@Accessors(chain = true)
public class EnumItemInfo {

    private String constant;
    private String value;
    private String desc;

}
