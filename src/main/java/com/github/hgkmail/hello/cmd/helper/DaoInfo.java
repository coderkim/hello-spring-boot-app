package com.github.hgkmail.hello.cmd.helper;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * DAO信息
 *
 * @author kimhuang
 * @date 2021/2/14
 */
@Data
@Accessors(chain = true)
public class DaoInfo {

    private String name;
    private String comment;
    private String author;
    private String date;
    private String repoAnnotation;
    private String criteriaName;
    private String criteriaVarName;
    private String providerName;

}
