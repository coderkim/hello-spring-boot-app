package com.github.hgkmail.hello.cmd;

import com.github.hgkmail.hello.cmd.helper.*;
import com.google.common.base.Splitter;
import com.thoughtworks.qdox.model.JavaClass;
import com.thoughtworks.qdox.model.JavaField;
import com.thoughtworks.qdox.model.JavaMethod;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.velocity.VelocityContext;

import javax.print.Doc;
import java.util.ArrayList;
import java.util.List;

import static java.lang.System.out;

/**
 * 生成字段枚举类
 *
 * @author kimhuang
 * @date 2021/2/12
 */
public class GenFieldEnum extends BaseParseClass {

    public static void main(String[] args) {
        new GenFieldEnum().run();
    }

    @Override
    public void run() {
        parseClass(new RenderXxx());
    }

    private class RenderXxx implements ParseClassListener {
        //定义元信息变量
        private List<EnumInfo> fieldEnums=new ArrayList<>();

        RenderXxx() {
        }

        @Override
        public void onStart(JavaClass jClass) {
        }

        @Override
        public void onField(JavaClass jClass, JavaField jField) {
            if (jField.getTagByName("enum")==null) {
                return;
            }

            EnumInfo enumInfo = new EnumInfo();
            String tagValue = DocTagUtil.firstTagValue(jField, "enum", "");
            if (StringUtils.isBlank(tagValue)) {
                return;
            }

            List<String> itemDefinitions = Splitter.on(",").omitEmptyStrings().trimResults().splitToList(tagValue);
            if (CollectionUtils.isEmpty(itemDefinitions)) {
                return;
            }

            for (String itemDefStr : itemDefinitions) {
                List<String> itemInfoStrs = Splitter.on(":").omitEmptyStrings().trimResults().splitToList(itemDefStr);
                if (CollectionUtils.isEmpty(itemInfoStrs)) {
                    continue;
                }
                EnumItemInfo itemInfo = new EnumItemInfo();
                itemInfo.setValue(DocTagUtil.getAt(itemInfoStrs, 0, ""));
                itemInfo.setDesc(DocTagUtil.getAt(itemInfoStrs, 1, ""));
                itemInfo.setConstant(StringUtils.upperCase(DocTagUtil.getAt(itemInfoStrs, 2, "")));

                enumInfo.getItems().add(itemInfo);
            }
            enumInfo.setName(jClass.getSimpleName()+DocTagUtil.toUpperCaseFirstOne(jField.getName()) +"Enum");
            enumInfo.setComment(DocTagUtil.firstTagValue(jClass, "comment", "")+jField.getName());
            enumInfo.setAuthor(DocTagUtil.firstTagValue(jClass, "author", ""));
            enumInfo.setDate(DocTagUtil.firstTagValue(jClass, "date", ""));

            fieldEnums.add(enumInfo);
        }

        @Override
        public void onMethod(JavaClass jClass, JavaMethod jMethod) {
        }

        @Override
        public void onEnd(JavaClass jClass) {
            renderVm("code-maker/generate/field_enum_java.vm", new RenderListener() {
                @Override
                public void beforeRender(VelocityContext vContext) {
                    vContext.put("fieldEnums", fieldEnums);
                }

                @Override
                public void afterRender(VelocityContext vContext, String result) {
                    out.println(result);
                }
            });
        }

    }

}
