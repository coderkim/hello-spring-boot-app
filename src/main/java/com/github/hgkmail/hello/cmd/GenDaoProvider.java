package com.github.hgkmail.hello.cmd;

import com.github.hgkmail.hello.cmd.helper.*;
import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.thoughtworks.qdox.model.JavaClass;
import com.thoughtworks.qdox.model.JavaField;
import com.thoughtworks.qdox.model.JavaMethod;
import org.apache.commons.lang3.StringUtils;
import org.apache.velocity.VelocityContext;

import javax.print.Doc;
import java.util.ArrayList;
import java.util.List;

import static java.lang.System.out;

/**
 * 解析类模板, copy用的
 *
 * @author kimhuang
 * @date 2021/2/12
 */
public class GenDaoProvider extends BaseParseClass {

    public static void main(String[] args) {
        new GenDaoProvider().run();
    }

    @Override
    public void run() {
        parseClass(new RenderDaoProvider());
    }

    private class RenderDaoProvider implements ParseClassListener {
        private DaoProviderClazzInfo clazz = new DaoProviderClazzInfo();
        private List<DaoProviderFieldInfo> generalFields = new ArrayList<>();

        RenderDaoProvider() {
        }

        @Override
        public void onStart(JavaClass jClass) {
            clazz.setName(jClass.getSimpleName());
            clazz.setDate(DocTagUtil.firstTagValue(jClass, "date", ""));
            clazz.setAuthor(DocTagUtil.firstTagValue(jClass, "author", ""));
            clazz.setComment(DocTagUtil.firstTagValue(jClass, "comment", ""));
            clazz.setTable(DocTagUtil.firstTagValue(jClass, "table", "t_"));
            clazz.setAllFields(new ArrayList<>());
        }

        @Override
        public void onField(JavaClass jClass, JavaField jField) {
            clazz.getAllFields().add(jField.getName());
            if (jField.getTagByName("cond")==null && jField.getTagByName("condRange")==null) {
                return;
            }

            if (jField.getTagByName("cond")!=null) {
                DaoProviderFieldInfo fieldInfo = new DaoProviderFieldInfo();
                fieldInfo.setName(DocTagUtil.firstTagValue(jField, "cond", jField.getName()));
                fieldInfo.setType(DocTagUtil.firstTagValue(jField, "condType", jField.getType().getSimpleName()));
                fieldInfo.setDefaultVal("").setHasDefault(false);
                fieldInfo.setCompareOperator("=");
                fieldInfo.setGetter(getter(fieldInfo.getName()));
                fieldInfo.setSetter(setter(fieldInfo.getName()));
                fieldInfo.setColumn(DocTagUtil.firstTagValue(jField, "column", jField.getName()));

                fieldInfo.setComment(DocTagUtil.firstTagValue(jField, "comment", ""));
                fieldInfo.setHasComment(false);
                if (StringUtils.isNotBlank(fieldInfo.getComment())) {
                    fieldInfo.setHasComment(true);
                }

                generalFields.add(fieldInfo);
            } else if (jField.getTagByName("condRange")!=null) {
                String startFieldName = jField.getName() + "Start";
                String endFieldName = jField.getName() + "End";
                String condRangeVal = DocTagUtil.firstTagValue(jField, "condRange", "");
                if (StringUtils.isNotBlank(condRangeVal)) {
                    List<String> condRangeVals = Splitter.on(",").trimResults().omitEmptyStrings().splitToList(condRangeVal);
                    startFieldName = DocTagUtil.getAt(condRangeVals, 0, startFieldName);
                    endFieldName = DocTagUtil.getAt(condRangeVals, 1, endFieldName);
                }

                DaoProviderFieldInfo startField = new DaoProviderFieldInfo();
                startField.setName(startFieldName);
                startField.setType(DocTagUtil.firstTagValue(jField, "condType", jField.getType().getSimpleName()));
                startField.setDefaultVal("").setHasDefault(false);
                startField.setComment(DocTagUtil.firstTagValue(jField, "comment", "")+" start");
                startField.setHasComment(true);
                startField.setCompareOperator(">");
                startField.setGetter(getter(startField.getName()));
                startField.setSetter(setter(startField.getName()));
                startField.setColumn(DocTagUtil.firstTagValue(jField, "column", jField.getName()));

                DaoProviderFieldInfo endField = new DaoProviderFieldInfo();
                endField.setName(endFieldName);
                endField.setType(DocTagUtil.firstTagValue(jField, "condType", jField.getType().getSimpleName()));
                endField.setDefaultVal("").setHasDefault(false);
                endField.setComment(DocTagUtil.firstTagValue(jField, "comment", "")+" end");
                endField.setHasComment(true);
                endField.setCompareOperator("<");
                endField.setGetter(getter(endField.getName()));
                endField.setSetter(setter(endField.getName()));
                endField.setColumn(DocTagUtil.firstTagValue(jField, "column", jField.getName()));

                generalFields.add(startField);
                generalFields.add(endField);
            }

        }

        private String setter(String fieldName) {
            StringBuilder sb=new StringBuilder();
            sb.append("set").append(DocTagUtil.toUpperCaseFirstOne(fieldName));
            return sb.toString();
        }

        private String getter(String fieldName) {
            StringBuilder sb=new StringBuilder();
            sb.append("get").append(DocTagUtil.toUpperCaseFirstOne(fieldName));
            return sb.toString();
        }

        @Override
        public void onMethod(JavaClass jClass, JavaMethod jMethod) {
            //nothing
        }

        @Override
        public void onEnd(JavaClass jClass) {
            clazz.setAllFieldsPart(Joiner.on(DocTagUtil.COMMA).skipNulls().join(clazz.getAllFields()));

            renderVm("code-maker/generate/dao_provider_java.vm", new RenderListener() {
                @Override
                public void beforeRender(VelocityContext vContext) {
                    vContext.put("clazz", clazz);
                    vContext.put("generalFields", generalFields);
                }

                @Override
                public void afterRender(VelocityContext vContext, String result) {
                    out.println(result);
                }
            });
        }

    }

}
