package com.github.hgkmail.hello.cmd;

import com.github.hgkmail.hello.cmd.helper.*;
import com.google.common.base.Splitter;
import com.thoughtworks.qdox.model.JavaClass;
import com.thoughtworks.qdox.model.JavaField;
import com.thoughtworks.qdox.model.JavaMethod;
import org.apache.commons.lang3.StringUtils;
import org.apache.velocity.VelocityContext;

import javax.print.Doc;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * 解析类模板, copy用的
 *
 * @author kimhuang
 * @date 2021/2/12
 */
public class GenDaoCriteria extends BaseParseClass {

    public static void main(String[] args) {
        new GenDaoCriteria().run();
    }

    @Override
    public void run() {
        parseClass(new RenderDaoCriteria());
    }

    private class RenderDaoCriteria implements ParseClassListener {
        private ClazzInfo clazz = new ClazzInfo();
        private List<FieldInfo> generalFields = new ArrayList<>();

        RenderDaoCriteria() {
        }

        @Override
        public void onStart(JavaClass jClass) {
            clazz = new ClazzInfo();
            clazz.setName(jClass.getSimpleName());
            clazz.setAuthor(DocTagUtil.firstTagValue(jClass, "author", ""));
            clazz.setComment(DocTagUtil.firstTagValue(jClass, "comment", ""));
            clazz.setDate(DocTagUtil.firstTagValue(jClass, "date", ""));
        }

        @Override
        public void onField(JavaClass jClass, JavaField jField) {
            if (jField.getTagByName("cond")==null && jField.getTagByName("condRange")==null) {
                return;
            }

            if (jField.getTagByName("cond")!=null) {
                FieldInfo fieldInfo = new FieldInfo();
                fieldInfo.setName(DocTagUtil.firstTagValue(jField, "cond", jField.getName()));
                fieldInfo.setType(DocTagUtil.firstTagValue(jField, "condType", jField.getType().getSimpleName()));
                fieldInfo.setDefaultVal("").setHasDefault(false);
                fieldInfo.setComment(DocTagUtil.firstTagValue(jField, "comment", ""));

                fieldInfo.setHasComment(false);
                if (StringUtils.isNotBlank(fieldInfo.getComment())) {
                    fieldInfo.setHasComment(true);
                }

                generalFields.add(fieldInfo);
            } else if (jField.getTagByName("condRange")!=null) {
                String startFieldName = jField.getName() + "Start";
                String endFieldName = jField.getName() + "End";
                String condRangeVal = DocTagUtil.firstTagValue(jField, "condRange", "");
                if (StringUtils.isNotBlank(condRangeVal)) {
                    List<String> condRangeVals = Splitter.on(",").trimResults().omitEmptyStrings().splitToList(condRangeVal);
                    startFieldName = DocTagUtil.getAt(condRangeVals, 0, startFieldName);
                    endFieldName = DocTagUtil.getAt(condRangeVals, 1, endFieldName);
                }

                FieldInfo startField = new FieldInfo();
                startField.setName(startFieldName);
                startField.setType(DocTagUtil.firstTagValue(jField, "condType", jField.getType().getSimpleName()));
                startField.setDefaultVal("").setHasDefault(false);
                startField.setComment(DocTagUtil.firstTagValue(jField, "comment", "")+" start");
                startField.setHasComment(true);

                FieldInfo endField = new FieldInfo();
                endField.setName(endFieldName);
                endField.setType(DocTagUtil.firstTagValue(jField, "condType", jField.getType().getSimpleName()));
                endField.setDefaultVal("").setHasDefault(false);
                endField.setComment(DocTagUtil.firstTagValue(jField, "comment", "")+" end");
                endField.setHasComment(true);

                generalFields.add(startField);
                generalFields.add(endField);
            }

        }

        @Override
        public void onMethod(JavaClass jClass, JavaMethod jMethod) {
            //nothing
        }

        @Override
        public void onEnd(JavaClass jClass) {
            renderVm("code-maker/generate/dao_criteria_java.vm", new RenderListener() {
                @Override
                public void beforeRender(VelocityContext vContext) {
                    vContext.put("clazz", clazz);
                    vContext.put("generalFields", generalFields);
                }

                @Override
                public void afterRender(VelocityContext vContext, String result) {
                    System.out.println(result);
                }
            });
        }

    }

}
