package com.github.hgkmail.hello.cmd.helper;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.List;

/**
 * class
 *
 * @author kimhuang
 * @date 2021/2/15
 */
@Data
@Accessors(chain = true)
public class EnumInfo {

    private String name;
    private String comment;
    private String author;
    private String date;
    private List<EnumItemInfo> items;

    public EnumInfo() {
        this.items = new ArrayList<>();
    }

}
