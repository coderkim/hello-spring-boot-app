package com.github.hgkmail.hello.cmd.helper;

import org.apache.velocity.VelocityContext;

/**
 * velocity渲染监听器
 *
 * @author kimhuang
 * @date 2021/2/15
 */
public interface RenderListener {

    /**
     * 渲染前设置变量
     */
    void beforeRender(VelocityContext vContext);

    /**
     * 渲染结果
     */
    void afterRender(VelocityContext vContext, String result);

}
