package com.github.hgkmail.hello.cmd.helper;

/**
 * 解析Java类源文件配置
 *
 * @author kimhuang
 * @date 2021/2/12
 */
public class ParseClassConfig {

    public static final String SOURCE_DIR = "/Users/hgkmail-mb/github/hello-spring-boot-app/src/main/java/com/github/hgkmail/hello/entity/generate";

    public static final String OUTPUT_DIR = "/Users/hgkmail-mb/Downloads";

    public static final String TARGET_CLASS = "com.github.hgkmail.hello.entity.generate.ShotTransferResourceLog";

    /**
     * 阿里手册建议 save/insert
     * save比insert好，save可以加上on duplicate key update name='xxx'
     */
    public static final String DAO_IDIOM_INSERT = "save";

    /**
     * 阿里手册建议 remove/delete
     * remove比delete好，remove可以表示软删除，即update deleted=1
     */
    public static final String DAO_IDIOM_DELETE = "remove";

    /**
     * DAO注解
     */
    public static final String DAO_IDIOM_REPOSITORY = "@MyBatisRepository";

    public static final String SUFFIX_LIST = "List";

}
