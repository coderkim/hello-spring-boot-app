package com.github.hgkmail.hello.cmd.helper;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 字段信息
 *
 * @author kimhuang
 * @date 2021/2/14
 */
@Data
@Accessors(chain = true)
public class FieldInfo {

    protected String name;
    protected String type;
    protected String comment;
    protected boolean hasComment;
    protected String defaultVal;
    protected boolean hasDefault;

}
