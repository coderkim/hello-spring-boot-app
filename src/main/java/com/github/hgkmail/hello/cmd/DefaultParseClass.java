package com.github.hgkmail.hello.cmd;

import com.github.hgkmail.hello.cmd.helper.*;
import com.thoughtworks.qdox.model.JavaClass;
import com.thoughtworks.qdox.model.JavaField;
import com.thoughtworks.qdox.model.JavaMethod;
import org.apache.velocity.VelocityContext;

import static java.lang.System.out;

/**
 * 解析类模板, copy用的
 *
 * @author kimhuang
 * @date 2021/2/12
 */
public class DefaultParseClass extends BaseParseClass {

    public static void main(String[] args) {
        new DefaultParseClass().run();
    }

    @Override
    public void run() {
        parseClass(new RenderXxx());
    }

    private class RenderXxx implements ParseClassListener {
        //定义元信息变量

        RenderXxx() {
        }

        @Override
        public void onStart(JavaClass jClass) {
        }

        @Override
        public void onField(JavaClass jClass, JavaField jField) {
        }

        @Override
        public void onMethod(JavaClass jClass, JavaMethod jMethod) {
        }

        @Override
        public void onEnd(JavaClass jClass) {
            renderVm("code-maker/generate/xxx.vm", new RenderListener() {
                @Override
                public void beforeRender(VelocityContext vContext) {
                    vContext.put("abc", 123);
                }

                @Override
                public void afterRender(VelocityContext vContext, String result) {
                    out.println(result);
                }
            });
        }

    }

}
