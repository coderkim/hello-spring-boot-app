package com.github.hgkmail.hello.cmd.helper;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 表信息
 *
 * @author kimhuang
 * @date 2021/2/13
 */
@Data
@Accessors(chain = true)
public class TableInfo {

    private String name;
    private String charset;
    private String comment;

}
