package com.github.hgkmail.hello.cmd.helper;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * VO 类信息
 *
 * @author kimhuang
 * @date 2021/2/15
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
public class ViewObjectFieldInfo extends FieldInfo {

    private String title;
    private String entityName;
    private String entityGetter;
    private String formatEntityGetter;
    private boolean hasFormatter;

}
