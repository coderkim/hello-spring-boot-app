package com.github.hgkmail.hello.leetcode101.pointer.linkedlist;

import com.github.hgkmail.hello.leetcode101.base.ListNode;

public class LC83RemoveDuplicatesFromSortedList {
    public void deleteNode(ListNode node, ListNode prev) {
        prev.next = node.next;
    }

    public ListNode deleteDuplicates(ListNode head) {
        //base
        if (head==null || head.next==null) {
            return head;
        }
        //dummyHead
        ListNode dummyHead=new ListNode(0, head);
        ListNode curr=head;
        ListNode prev=dummyHead;
        while (curr!=null && curr.next!=null) {
            if (curr.val==curr.next.val) {
                deleteNode(curr, prev); //prev不用变
            } else {
                prev=curr;
            }
            curr=curr.next;
        }

        return dummyHead.next;
    }

    public static void main(String[] args) {

    }
}
