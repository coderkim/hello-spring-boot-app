package com.github.hgkmail.hello.leetcode101.collection.stack;

import java.util.Stack;

public class LC232ImplementQueueUsingStacks {
    //使用2个stack来模拟Queue，在出队时把数据从一个栈倒到另一个栈
    class MyQueue {
        private Stack<Integer> in, out;
        public MyQueue() {
            in = new Stack<>();
            out = new Stack<>();
        }

        public void push(int x) {
            in.push(x);
        }

        public int pop() {
            //如果out是空，把in倒到out，颠倒一次顺序
            if (out.isEmpty()) {
                while (!in.isEmpty()) {
                    out.push(in.pop());
                }
            }
            //如果out不是空，直接弹出
            return out.pop();
        }

        public int peek() {
            //如果out是空，把in倒到out，颠倒一次顺序
            if (out.isEmpty()) {
                while (!in.isEmpty()) {
                    out.push(in.pop());
                }
            }
            return out.peek();
        }

        public boolean empty() {
            return out.isEmpty() && in.isEmpty();
        }
    }

    public static void main(String[] args) {

    }
}
