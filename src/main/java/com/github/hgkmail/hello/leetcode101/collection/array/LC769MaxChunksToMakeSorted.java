package com.github.hgkmail.hello.leetcode101.collection.array;

public class LC769MaxChunksToMakeSorted {
    //从左往右遍历，记录当前最大值，当最大值与下标相等时，可以多一次分割
    //最大值不会小于当前下标，因为只包含0~n，且每个数都不一样
    //最大值大于当前下标时，说明小的数在后面，不能分割
    public int maxChunksToSorted(int[] arr) {
        int n=arr.length;
        if (n<=0) {
            return 0;
        }
        int res=0;
        int currMax=0;
        for (int i = 0; i < n; i++) {
            currMax=Math.max(currMax, arr[i]);
            if (currMax==i) {
                res++;
            }
        }
        return res;
    }

    public static void main(String[] args) {

    }
}
