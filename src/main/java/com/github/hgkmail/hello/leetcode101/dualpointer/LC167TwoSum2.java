package com.github.hgkmail.hello.leetcode101.dualpointer;

public class LC167TwoSum2 {
    public int[] twoSum(int[] numbers, int target) {
        //base case
        if (numbers.length<=0) {
            return new int[]{};
        }
        //dual pointer 双指针思想
        int left=0;
        int right=numbers.length-1;
        int sum;
        while (left<right) {
            sum=numbers[left]+numbers[right];
            if (sum==target) {
                return new int[]{left+1,right+1};
            } else if (sum<target) {
                left+=1;
            } else {
                right-=1;
            }
        }
        //没找到
        return new int[]{};
    }

    public static void main(String[] args) {

    }
}
