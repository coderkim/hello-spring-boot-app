package com.github.hgkmail.hello.leetcode101.greedy;

public class LC605CanPlaceFlowers {

    public boolean canPlaceFlowers(int[] flowerbed, int n) {
        //base case
        if (flowerbed.length<=0) {
            return false;
        }
        int curr=0;
        int num=0;
        //从左往右
        while (curr<flowerbed.length) {
            if (flowerbed[curr]==1) {
                curr=curr+2;
            } else {
                if (curr+1>= flowerbed.length || flowerbed[curr+1]==0) {
                    num+=1;
                    curr=curr+2;
                } else {
                    curr=curr+3;
                }
            }
        }

        return num>=n;
    }

    public static void main(String[] args) {

    }

}
