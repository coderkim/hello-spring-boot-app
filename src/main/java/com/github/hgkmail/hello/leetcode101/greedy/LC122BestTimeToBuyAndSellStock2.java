package com.github.hgkmail.hello.leetcode101.greedy;

public class LC122BestTimeToBuyAndSellStock2 {
    public int maxProfit(int[] prices) {
        //base case
        if (prices.length<=0) {
            return 0;
        }
        int profit=0;
        for (int i = 1; i < prices.length; i++) {
            if (prices[i]>prices[i-1]) {
                profit+=prices[i]-prices[i-1];
            }
        }
        return profit;
    }

    public static void main(String[] args) {

    }
}
