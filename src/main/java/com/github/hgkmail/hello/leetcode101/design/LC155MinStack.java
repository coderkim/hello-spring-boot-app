package com.github.hgkmail.hello.leetcode101.design;

import java.util.Stack;

//这种min或max数据结构题目，准备2个一样的数据结构，比如这里2个stack，一个用来存当前的最值。
public class LC155MinStack {
    class MinStack {
        //存数据
        Stack<Integer> stack;
        //存最值
        Stack<Integer> min;

        public MinStack() {
            stack = new Stack<>();
            min=new Stack<>();
        }

        public void push(int val) {
            if (stack.isEmpty()) {
                stack.push(val);
                min.push(val);
                return;
            }
            int top = min.peek();
            stack.push(val);
            min.push(Math.min(top, val)); // 同时更新最值
        }

        public void pop() {
            if (stack.isEmpty()) {
                return;
            }
            stack.pop();
            min.pop();
        }

        public int top() {
            return stack.peek();
        }

        public int getMin() {
            return min.peek();
        }
    }

    public static void main(String[] args) {

    }
}
