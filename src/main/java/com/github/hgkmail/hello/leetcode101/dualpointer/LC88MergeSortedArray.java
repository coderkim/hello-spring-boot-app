package com.github.hgkmail.hello.leetcode101.dualpointer;

public class LC88MergeSortedArray {
    public void merge(int[] nums1, int m, int[] nums2, int n) {
        int i=m-1; //nums1 指针
        int j=n-1; //nums2 指针
        int k=m+n-1; //合并数组 指针
        while (i>=0 && j>=0) {
            if (nums1[i]<=nums2[j]) {
                nums1[k]=nums2[j];
                j-=1;
            } else {
                nums1[k]=nums1[i];
                i-=1;
            }
            k-=1;
        }
        //如果nums2还有剩下元素，要处理下
        if (j>=0) {
            while (j>=0) {
                nums1[k]=nums2[j];
                j-=1;
                k-=1;
            }
        }
    }

    public static void main(String[] args) {

    }
}
