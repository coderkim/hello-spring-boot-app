package com.github.hgkmail.hello.leetcode101.collection.array;

import java.util.ArrayList;
import java.util.List;

//数组创建桶，桶排序
public class LC448FindAllNumbersDisappearedInAnArray {
    public List<Integer> findDisappearedNumbers(int[] nums) {
        int n = nums.length;
        int[] counts = new int[n+1]; //int动态数组默认值为0
        for (int i = 0; i < n; i++) {
            int num = nums[i];
            counts[num]+=1;
        }
        List<Integer> res = new ArrayList<>();
        for (int i = 1; i <= n; i++) {
            if (counts[i]==0) {
                res.add(i);
            }
        }
        return res;
    }

    public static void main(String[] args) {
        System.out.println(new LC448FindAllNumbersDisappearedInAnArray().findDisappearedNumbers(new int[]{1,1}));
    }
}
