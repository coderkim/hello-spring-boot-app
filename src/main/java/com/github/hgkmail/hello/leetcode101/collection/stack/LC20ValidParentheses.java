package com.github.hgkmail.hello.leetcode101.collection.stack;

import java.util.HashMap;
import java.util.Stack;

//用栈进行匹配
public class LC20ValidParentheses {
    public boolean isValid(String s) {
        char[] chars = s.toCharArray();
        if (chars.length<=0) {
            return true;
        }
        HashMap<Character, Character> map = new HashMap<>();
        map.put('(', ')');
        map.put('{', '}');
        map.put('[', ']');
        Stack<Character> stack = new Stack<>();
        for (int i = 0; i < chars.length; i++) {
            if (stack.isEmpty()) {
                stack.push(chars[i]);
                continue;
            }
            char topChar = stack.peek();
            if (map.getOrDefault(topChar, ' ')==chars[i]) {
                stack.pop();
            } else {
                stack.push(chars[i]);
            }
        }

        return stack.isEmpty();
    }

    public static void main(String[] args) {

    }
}
