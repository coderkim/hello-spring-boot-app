package com.github.hgkmail.hello.leetcode101.dualpointer;

import com.github.hgkmail.hello.leetcode101.base.ListNode;

public class LC141LinkedListCycle {
    public boolean hasCycle(ListNode head) {
        ListNode fast=head;
        ListNode slow=head;
        do {
            //fast走到尽头，说明不存在环
            if (fast==null || fast.next==null) {
                return false;
            }
            fast=fast.next.next; //快指针走2步
            slow=slow.next; //慢指针走1步
        } while (fast!=slow);
        //fast和slow相遇，说明存在环
        return true;
    }

    public static void main(String[] args) {

    }
}
