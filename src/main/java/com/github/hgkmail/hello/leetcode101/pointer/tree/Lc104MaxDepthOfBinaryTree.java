package com.github.hgkmail.hello.leetcode101.pointer.tree;

import guru.nidi.graphviz.attribute.*;
import guru.nidi.graphviz.engine.Format;
import guru.nidi.graphviz.engine.Graphviz;
import guru.nidi.graphviz.model.Graph;

import java.io.File;
import java.io.IOException;

import static guru.nidi.graphviz.attribute.Attributes.attr;
import static guru.nidi.graphviz.attribute.Rank.RankDir.TOP_TO_BOTTOM;
import static guru.nidi.graphviz.model.Factory.graph;
import static guru.nidi.graphviz.model.Factory.node;
import static guru.nidi.graphviz.model.Link.to;

/**
 * 一棵树要么是空树，要么有两个指针，每个指针指向一棵树。树是一种【递归结构】，很多树的问题可以使用【递归】来处理。
 *
 * @author kimhuang
 * @date 2021/7/6
 */
public class Lc104MaxDepthOfBinaryTree {

    public class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode() {
        }

        TreeNode(int val) {
            this.val = val;
        }

        TreeNode(int val, TreeNode left, TreeNode right) {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }

    public int maxDepth(TreeNode root) {
        if (root==null) {
            return 0;
        }
        //有节点就有1高度
        return Math.max(maxDepth(root.left),maxDepth(root.right))+1;
    }

    public static void main(String[] args) throws IOException {
        Graph g = graph("example1").directed()
                .graphAttr().with(Rank.dir(TOP_TO_BOTTOM))
                .nodeAttr().with(Font.name("Arial"))
                .linkAttr().with("class", "link-class")
                .with(
                        node("a1").with(Label.of("10")).with(Color.RED).link(node("b2").with(Label.of("8"))),
                        node("a1").link(
                                to(node("c3").with(Label.of("16"))).with(attr("weight", 5), Style.DASHED)
                        ),
                        node("b2").link(node("d").with(Label.of("5"))),
                        node("b2").link(node("e").with(Label.of("9")).with(Color.ORANGE)),
                        node("c3").link(node("f").with(Label.of("11"))),
                        node("c3").link(node("g").with(Label.of("20")))
                );
        Graphviz.fromGraph(g).height(100).render(Format.SVG).toFile(new File("example/ex1.svg"));
    }

}
