package com.github.hgkmail.hello.leetcode101.greedy;

import java.util.Arrays;

/**
 * https://leetcode.cn/problems/assign-cookies
 */
public class LC455AssignCookies {
    public int findContentChildren(int[] g, int[] s) {
        int num=0;
        int begin=0;
        boolean found;
        Arrays.sort(g);
        Arrays.sort(s);
        for (int i = 0; i < g.length; i++) {
            found=false;
            //开始找饼干
            for (int j = begin; j < s.length; j++) {
                //若找到了
                if (g[i]<=s[j]) {
                    num+=1;
                    found=true;
                    begin=j+1;
                    break;
                }
            }
            if (!found) {
                break;
            }
        }

        return num;
    }

    public int findContentChildren2(int[] g, int[] s) {
        Arrays.sort(g);
        Arrays.sort(s);
        int childNum=0;
        for (int i = 0; i < s.length; i++) {
            if (childNum<g.length && g[childNum]<=s[i]) {
                childNum+=1;
            }
        }

        return childNum;
    }

    public static void main(String[] args) {
        int[] arr={1,5,2,3,-1};
        System.out.println(Arrays.toString(arr));
        Arrays.sort(arr);
        System.out.println(Arrays.toString(arr));
    }
}
