package com.github.hgkmail.hello.leetcode101.pointer.tree;

import com.github.hgkmail.hello.leetcode101.base.CommonUtil;
import com.github.hgkmail.hello.leetcode101.base.TreeNode;

public class LC101SymmetricTree {
    //不需要全局状态，使用dfs，不需要用backtrack
    public boolean dfs(TreeNode root1, TreeNode root2) {
        if (root1==null && root2==null) {
            return true;
        }
        if (root1==null || root2==null) {
            return false;
        }
        if (root1.val!=root2.val) {
            return false;
        }
        return dfs(root1.left, root2.right) && dfs(root1.right, root2.left);
    }
    public boolean isSymmetric(TreeNode root) {
        if (root==null) {
            return true;
        }
        return dfs(root.left, root.right);
    }

    public static void main(String[] args) {
        TreeNode root= CommonUtil.deserializeBinaryTree("1,2,3,#,#,4,#,#,2,4,#,#,3,#,#");
        System.out.println(new LC101SymmetricTree().isSymmetric(root));
    }
}
