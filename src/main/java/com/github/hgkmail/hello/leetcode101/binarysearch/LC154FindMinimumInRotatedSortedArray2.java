package com.github.hgkmail.hello.leetcode101.binarysearch;

public class LC154FindMinimumInRotatedSortedArray2 {
    public int findMin(int[] nums) {
        if (nums.length==1) {
            return nums[0];
        }
        int l=0;
        int r=nums.length-1;
        int mid;
        int min=Integer.MAX_VALUE;
        while (l<=r) {
            mid=l+(r-l)/2;
            if (nums[mid]==nums[l]) {//无法判断有序性
                min=Math.min(min, nums[mid]);
                l++;
            } else if (nums[mid]==nums[r]) {//无法判断有序性
                min=Math.min(min, nums[mid]);
                r--;
            } else if (nums[mid]>nums[l]) { //左边有序
                min=Math.min(min, nums[l]);
                l=mid+1;
            } else { //右边有序
                min=Math.min(min, nums[mid]);
                r=mid-1;
            }
        }
        return min;
    }

    public static void main(String[] args) {
        System.out.println(new LC154FindMinimumInRotatedSortedArray2().findMin(new int[]{10,1,10,10,10}));
    }
}
