package com.github.hgkmail.hello.leetcode101.greedy;

import java.util.Arrays;
import java.util.Comparator;

public class LC452MinimumNumberOfArrowsToBurstBalloons {
    public int findMinArrowShots(int[][] points) {
        //base case
        if (points.length<=0) {
            return 0;
        }
        //按区间尾部升序排序
        Arrays.sort(points, new Comparator<int[]>() {
            @Override
            public int compare(int[] o1, int[] o2) {
//                return o1[1]-o2[1]; //有可能溢出。。。
                if (o1[1]==o2[1]) {
                    return 0;
                } else if (o1[1]>o2[1]) {
                    return 1;
                } else {
                    return -1;
                }
            }
        });
        int removed=0;
        int prev=points[0][1];
        for (int i = 1; i < points.length; i++) {
            if (points[i][0]<=prev) {
                removed+=1;
            } else {
                prev=points[i][1];
            }
        }

        return points.length-removed;
    }

    public static void main(String[] args) {
        System.out.println(new LC452MinimumNumberOfArrowsToBurstBalloons().findMinArrowShots(new int[][]{{-2147483646,-2147483645},{2147483646,2147483647}}));
    }
}
