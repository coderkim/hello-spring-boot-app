package com.github.hgkmail.hello.leetcode101.pointer.linkedlist;

import com.github.hgkmail.hello.leetcode101.base.ListNode;

public class LC21MergeTwoSortedLists {
    public ListNode mergeTwoLists(ListNode list1, ListNode list2) {
        if (list1==null) {
            return list2;
        }
        if (list2==null) {
            return list1;
        }
        ListNode dummyHead = new ListNode(0);
        ListNode curr = dummyHead;
        ListNode i=list1;
        ListNode j=list2;
        while (i!=null && j!=null) {
            if (i.val<= j.val) {
                curr.next = i;
                i = i.next;
            } else {
                curr.next = j;
                j=j.next;
            }
            curr=curr.next;
        }
        //不用再合并，直接链接上即可
        curr.next = i!=null ? i : j;

        return dummyHead.next;
    }

    public static void main(String[] args) {

    }
}
