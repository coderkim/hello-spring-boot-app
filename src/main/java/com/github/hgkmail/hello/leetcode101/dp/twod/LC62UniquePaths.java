package com.github.hgkmail.hello.leetcode101.dp.twod;

import java.util.Arrays;

//典型的二维数组dp，子问题是以p(i,j)结尾的子图，必须包含p(i,j)
public class LC62UniquePaths {
    public int uniquePaths(int m, int n) {
        if (m<=0 || n<=0) {
            return 0;
        }
        int[][] dp=new int[m][n];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (i==0 && j==0) {
                    dp[i][j]=1;
                    continue;
                }
                if (i==0) {
                    dp[i][j]=dp[i][j-1];
                    continue;
                }
                if (j==0) {
                    dp[i][j]=dp[i-1][j];
                    continue;
                }
                dp[i][j]=dp[i-1][j]+dp[i][j-1];
            }
        }
        return dp[m-1][n-1];
    }

    public static void main(String[] args) {
        System.out.println(new LC62UniquePaths().uniquePaths(3,7));
    }
}
