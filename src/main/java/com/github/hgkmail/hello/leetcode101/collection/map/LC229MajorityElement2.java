package com.github.hgkmail.hello.leetcode101.collection.map;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class LC229MajorityElement2 {
    public List<Integer> majorityElement(int[] nums) {
        if (nums.length<=0) {
            return new ArrayList<>();
        }
        Map<Integer, Integer> map = new HashMap<>(nums.length);
        for (int i = 0; i < nums.length; i++) {
            map.put(nums[i], map.getOrDefault(nums[i], 0)+1);
        }
        return map.keySet().stream().filter(k->map.get(k)> nums.length/3).collect(Collectors.toList());
    }

    public static void main(String[] args) {

    }
}
