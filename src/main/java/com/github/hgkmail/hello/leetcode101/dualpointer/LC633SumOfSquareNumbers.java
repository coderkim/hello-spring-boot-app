package com.github.hgkmail.hello.leetcode101.dualpointer;

public class LC633SumOfSquareNumbers {
    public boolean judgeSquareSum(int c) {
        //范围不能取0~c，要么导致int溢出，要么执行超时
        //范围应该取0~sqrt(c)，square root 平方根
        long i=0;
        long j=(int)Math.sqrt(c);
        long res;
        while (i<=j) {
            res=i*i+j*j;
            if (res==c) {
                return true;
            } else if (res>c) {
                j-=1;
            } else {
                i+=1;
            }
        }
        return false;
    }

    public static void main(String[] args) {
        System.out.println(new LC633SumOfSquareNumbers().judgeSquareSum(1000000));
    }
}
