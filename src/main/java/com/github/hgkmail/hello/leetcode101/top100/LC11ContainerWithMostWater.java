package com.github.hgkmail.hello.leetcode101.top100;

//双指针代码框架都懂，但这道题不一定能想到用双指针，题做多了才有思路。
//双指针解法：从最左边和最右边往里面移动，移动规则——小的边移动而大的边不动
//因为容量是由小的边决定的，大的边移动后高度不变且宽度减小，容量肯定减小，是[无效移动]；小的边移动是有效移动，可以找到更大的容量。
public class LC11ContainerWithMostWater {
    public int maxArea(int[] height) {
        int len=height.length;
        if (len<=0) {
            return 0;
        }
        int i=0;
        int j=len-1;
        int maxArea=0;
        while (i<j) {
            //计算最大面积
            maxArea = Math.max(maxArea, Math.min(height[i], height[j])*(j-i));
            //移动小的边
            if (height[i]<=height[j]) {
                i++;
            } else {
                j--;
            }
        }
        return maxArea;
    }
    public static void main(String[] args) {
        System.out.println(new LC11ContainerWithMostWater().maxArea(new int[]{1,8,6,2,5,4,8,3,7}));
    }
}
