#!/usr/bin/env bash

docker build -t hello-spring-boot-app .

docker tag hello-spring-boot-app:latest registry.cn-hangzhou.aliyuncs.com/kimgroup/hello-spring-boot-app:latest

docker push  registry.cn-hangzhou.aliyuncs.com/kimgroup/hello-spring-boot-app:latest

#docker run -d -p 8085:8085 --net mysql-net --name hello-spring-boot-app registry.cn-hangzhou.aliyuncs.com/kimgroup/hello-spring-boot-app:latest
