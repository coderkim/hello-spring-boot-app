FROM java:8

MAINTAINER kimhuang <hgkformap@gmail.com>

EXPOSE 8085

VOLUME /tmp
ADD build/libs/hello-spring-boot-app-0.0.1-SNAPSHOT.jar boot-docker.jar

RUN sh -c 'touch /boot-docker.jar'
ENV JAVA_OPTS=""
ENTRYPOINT [ "sh", "-c", "java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar /boot-docker.jar --spring.profiles.active=prod" ]
